<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gerency extends Model
{
    protected $table = "gerency";
    protected $fillable=['name','direction'];
}
