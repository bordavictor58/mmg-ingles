<?php

namespace App\Exports\Participant;

use App\Inscription;
use App\LoginDaily;
use Auth;
use Carbon\Carbon;
use DateTime;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;

class ReportGeneral implements FromCollection, WithHeadings, ShouldAutoSize, WithMapping,WithStyles, WithColumnFormatting
{
    protected $start;
    protected $end;
    protected $course_id;

    public function __construct(string $start, string $end, int $course_id)
    {
        $this->date_start = Carbon::parse($start)->format( 'Y-m-d');
        $this->date_end = Carbon::parse($end)->format( 'Y-m-d');
        $this->course_id = $course_id;
    }
    public function collection()
    {
        return Inscription::with(['participant.company'])
        ->whereHas('classroom', function ($query) {
            $query
            ->when($this->course_id  > 0, function ($q) {
                return $q->where('course_id', $this->course_id);
            });

        })

        ->whereHas('participant',function($q){
            $q->whereNotIn('company_id',[1]);
        })
        ->active()
        ->get();
    }
    public function map($inscription): array
    {
        $data=[];
        //datos
            array_push($data,$inscription->participant->dni,$inscription->first_access,$inscription->classroom->updated_at,$inscription->participant->last_name,$inscription->participant->name,$inscription->classroom->course->name,$inscription->grade);
        //RETORNAMOS EL MAP
        return $data;

    }
    public function headings(): array
    {
        return [
            'DNI/CE','FECHA INGRESO','FECHA EXAMEN','APELLIDOS','NOMBRES','CURSO','NOTA'
        ];
    }

    public function columnFormats(): array
    {
        return [
            'A' => NumberFormat::FORMAT_TEXT,
            'B' => NumberFormat::FORMAT_TEXT,
            'C' => NumberFormat::FORMAT_TEXT,
            'D' => NumberFormat::FORMAT_TEXT,
            'E' => NumberFormat::FORMAT_TEXT,
            'F' => NumberFormat::FORMAT_TEXT,
            'G' => NumberFormat::FORMAT_TEXT,
            'H' => NumberFormat::FORMAT_TEXT,

        ];
    }
    public function styles(Worksheet $sheet)
    {
        $styleArray = [
            'font' => [
                'bold' => true,
                'color' => ['rgb' => 'FFFFFF'],
            ],

            'fill' => [
                'fillType' => Fill::FILL_SOLID,
                'startColor' => [
                    'argb' => '2543BC',
                ],
            ],
        ];
        return [
            // Style the first row as bold text.
            1 =>$styleArray,
        ];
    }
}
