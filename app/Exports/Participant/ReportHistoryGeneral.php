<?php

namespace App\Exports;

use DB;
use App\User;
use App\TestUser;
use Carbon\Carbon;
use App\Inscription;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;

class ReportHistoryGeneral implements FromCollection, WithHeadings, ShouldAutoSize, WithMapping,WithStyles, WithColumnFormatting
{
    protected $start;
    protected $end;
    protected $course_id;

    public function __construct(string $start, string $end, int $course_id)
    {
        $this->date_start = Carbon::parse($start)->format( 'Y-m-d H:i:s');
        $this->date_end = Carbon::parse($end)->format( 'Y-m-d H:i:s');
        $this->course_id = $course_id;
    }
    public function collection()
    {
        //seleccionamos el filtro para poder editar

        return Inscription::query()
        ->whereHas('classroom', function ($query) {
            $query->when($this->course_id  > 0, function ($q) {
                return $q->where('course_id', $this->course_id);
            });
        })
        ->whereHas('participant',function($q){
            $q->whereNotIn('company_id',[1]);
        })
        ->whereDate('updated_at','>=',$this->date_start)
        ->whereDate('updated_at','<=',$this->date_end)
        ->active()
        ->whereNotNull('grade_date')
        ->get();
    }
    public function map($inscription): array
    {
        $hour=Carbon::parse($inscription->last_date)->format( 'H:i:s');
        $date=Carbon::parse($inscription->last_date)->format( 'Y-m-d');
        $data=[];
        $inscription::where('updated_at', '!=' ,$inscription->last_date)
                            ->update(['updated_at' => $inscription->last_date]);

            array_push($data,$inscription->participant->cod_participante,$inscription->participant->dni,$inscription->participant->name,$inscription->participant->last_name,$inscription->participant->area,$inscription->grade,$inscription->classroom->name,$date,$hour,$inscription->classroom->hour,$inscription->classroom->facilitator->full_name,$inscription->condition);
        //RETORNAMOS EL MAP
        return $data;


    }
    public function headings(): array
    {
        return [
            'REGISTRO TRABAJADOR','DNI','NOMBRES','APELLIDOS','AREA',
            'NOTA','NOMBRE CURSO','FECHA QUE SE COMPLETO EL CURSO',' HORA QUE SE COMPLETO EL CURSO','DURACIÓN','NOMBRE DEL INSTRUCTOR','CONDICIÓN'
        ];

    }

    public function columnFormats(): array
    {
        return [
            'A' => NumberFormat::FORMAT_TEXT,
            'B' => NumberFormat::FORMAT_TEXT,
            'C' => NumberFormat::FORMAT_TEXT,
            'D' => NumberFormat::FORMAT_TEXT,
            'E' => NumberFormat::FORMAT_TEXT,
            'F' => NumberFormat::FORMAT_TEXT,
            'G' => NumberFormat::FORMAT_TEXT,

        ];
    }
    public function styles(Worksheet $sheet)
    {
        $styleArray = [
            'font' => [
                'bold' => true,
                'color' => ['rgb' => 'FFFFFF'],
            ],

            'fill' => [
                'fillType' => Fill::FILL_SOLID,
                'startColor' => [
                    'argb' => '33425E',
                ],
            ],
        ];
        return [
            // Style the first row as bold text.
            1 =>$styleArray,
        ];
    }
}

