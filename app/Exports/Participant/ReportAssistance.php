<?php

namespace App\Exports\Participant;

use DB;
use App\User;
use App\TestUser;
use Carbon\Carbon;
use App\Inscription;
use DateTime;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;

class ReportAssistance implements FromCollection, WithHeadings, ShouldAutoSize, WithMapping,WithStyles, WithColumnFormatting
{
    protected $start;
    protected $end;
    protected $course_id;

    public function __construct(string $start, string $end, int $course_id)
    {
        $this->date_start = Carbon::parse($start)->format( 'Y-m-d H:i:s');
        $this->date_end = Carbon::parse($end)->format( 'Y-m-d H:i:s');
        $this->course_id = $course_id;
    }
    public function collection()
    {
        //seleccionamos el filtro para poder editar

        return Inscription::query()
        ->whereHas('classroom', function ($query) {
            $query->when($this->course_id  > 0, function ($q) {
                return $q->where('course_id', $this->course_id);
            });
        })
        ->whereHas('participant',function($q){
            $q->whereNotIn('company_id',[1]);
        })
        ->whereDate('updated_at','>=',$this->date_start)
        ->whereDate('updated_at','<=',$this->date_end)
        ->active()
        ->whereNotNull('grade_date')
        ->get();
    }
    public function map($inscription): array
    {
        $data=[];

        $date='';
        $bool=isset($inscription->loginDailies->created_at);

        $inscription::where('updated_at', '!=' ,$inscription->last_date)
                            ->update(['updated_at' => $inscription->last_date]);

        if($bool)
        {
            $date=$inscription->loginDailies->created_at->format('Y-m-d H:i:s');
        }
        else{

            $date = new DateTime($inscription->first_date);
            $date->modify('-'.$inscription->time_course.' second')->format('Y-m-d H:i:s');
        }

        $inscription::where('updated_at', '!=' ,$inscription->last_date)
                            ->update(['updated_at' => $inscription->last_date]);

            array_push($data,$inscription->classroom->name,$inscription->participant->dni,$inscription->participant->firstlastname,$inscription->participant->middle_name,
                    $inscription->participant->name,$inscription->participant->position,$inscription->participant->area,$date,'Asistio',
                    $inscription->grade,$inscription->condition);
        //RETORNAMOS EL MAP
        return $data;

    }
    public function headings(): array
    {
        return [
            'CURSO','DNI',
            'APELLIDO PATERNO','APELLIDO MATERNO',
            'NOMBRES','CARGO','AREA',
            'FECHA Y HORA DEL PRIMER INGRESO',
            'ASISTENCIA','NOTA','CONDICIÓN'
        ];
    }

    public function columnFormats(): array
    {
        return [
            'A' => NumberFormat::FORMAT_TEXT,
            'B' => NumberFormat::FORMAT_TEXT,
            'C' => NumberFormat::FORMAT_TEXT,
            'D' => NumberFormat::FORMAT_TEXT,
            'E' => NumberFormat::FORMAT_TEXT,
            'F' => NumberFormat::FORMAT_TEXT,
            'G' => NumberFormat::FORMAT_TEXT,

        ];
    }
    public function styles(Worksheet $sheet)
    {
        $styleArray = [
            'font' => [
                'bold' => true,
                'color' => ['rgb' => 'FFFFFF'],
            ],

            'fill' => [
                'fillType' => Fill::FILL_SOLID,
                'startColor' => [
                    'argb' => '33425E',
                ],
            ],
        ];
        return [
            // Style the first row as bold text.
            1 =>$styleArray,
        ];
    }
}

