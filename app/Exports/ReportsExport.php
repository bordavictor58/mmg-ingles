<?php

namespace App\Exports;

use DB;
use App\Bank;
use App\User;
use App\TestUser;
use Carbon\Carbon;

use App\Inscription;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;


class ReportsExport implements FromCollection, WithHeadings, ShouldAutoSize, WithMapping, WithColumnFormatting
{
    protected $start;
    protected $end;
    protected $course_id;

    public function __construct(string $start, string $end, int $course_id)
    {
        $this->start = $start;
        $this->end = $end;
        $this->course_id = $course_id;
    }

    public function collection()
    {
        //seleccionamos el filtro para poder editar
        $date_start=Carbon::parse($this->start)->format('Y-m-d H:i:s');
        $date_end=Carbon::parse($this->end)->format('Y-m-d H:i:s');

        $inscription=Inscription::query()
        ->whereHas('classroom')
        ->whereNotIn('id', [8061,8063,8063,8067,8062,8064,8066,8068])
        ->whereNotNull('grade_date')
        ->get();

        //editamos si el campos updated_at es diferente a inscription->last_date
        foreach($inscription as $inscriptions)
        {
            Inscription::where('id',$inscriptions->id)
                        ->where('updated_at', '!=' ,$inscriptions->last_date)
                        ->update(['updated_at' => $inscriptions->last_date]);
        }
        //returnamos con los filtros
        return Inscription::query()
        ->whereHas('classroom', function ($query) {
            $query->when($this->course_id  > 0, function ($q) {
                return $q->where('course_id', $this->course_id)
                ->whereNotIn('course_id',[1,2,3,4]);
            });
        })
        ->whereDate('updated_at','>=',$date_start)
        ->whereDate('updated_at','<=',$date_end)
        ->whereNotIn('user_id', [2,5747,5251,5238,5746])
        ->active()
        ->whereNotNull('grade_date');
    }
    public function map($testuser): array
    {
        //$inscriptions=Inscription::where(['user_id'=>$testuser->user_id 'test_id'])->get();

        $header=[];

        $bank_answer=Bank::whereIn('id',$testuser->answers)->get();
        $bank_correct_answer=Bank::whereIn('id',$testuser->correct_answers)->get();
        $bank_questions=Bank::whereIn('id',$testuser->questions)->get();

        $count =  count($bank_questions);

        for ($i = 0; $i < $count; $i++) {

            array_push($header, $bank_questions[$i]->title,$bank_answer[$i]->title,$bank_correct_answer[$i]->title);

        }

        $data = [
            //Date::dateTimeToExcel($testuser->created_at),
            $testuser->test->classroom->name,
            $testuser->participant->dni,
            $testuser->participant->last_name,
            $testuser->participant->name,

        ];

        $data2 = [
            $testuser->grade
        ];

        $result = array_merge($data,$header,$data2);
        return $result;

    }
    public function headings(): array
    {
        return [
            'CURSO','DNI',
            'APELLIDOS', 'NOMBRES',
            'PREGUNTA 1',
            'OPCION MARCADA',
            'OPCION CORRECTA',
            'PREGUNTA 2',
            'OPCION MARCADA',
            'OPCION CORRECTA',
            'PREGUNTA 3',
            'OPCION MARCADA',
            'OPCION CORRECTA',
            'PREGUNTA 4',
            'OPCION MARCADA',
            'OPCION CORRECTA',
            'PREGUNTA 5',
            'OPCION MARCADA',
            'OPCION CORRECTA',
            'PREGUNTA 6',
            'OPCION MARCADA',
            'OPCION CORRECTA',
            'PREGUNTA 7',
            'OPCION MARCADA',
            'OPCION CORRECTA',
            'PREGUNTA 8',
            'OPCION MARCADA',
            'OPCION CORRECTA',
            'PREGUNTA 9',
            'OPCION MARCADA',
            'OPCION CORRECTA',
            'PREGUNTA 10',
            'OPCION MARCADA',
            'OPCION CORRECTA',
            'NOTA FINAL'
        ];
    }

    public function columnFormats(): array
    {
        return [
            'A' => NumberFormat::FORMAT_TEXT,
            'B' => NumberFormat::FORMAT_TEXT,
            'C' => NumberFormat::FORMAT_TEXT,
            'D' => NumberFormat::FORMAT_TEXT,
            'E' => NumberFormat::FORMAT_TEXT,
            'F' => NumberFormat::FORMAT_TEXT,
            'G' => NumberFormat::FORMAT_TEXT,

        ];
    }

    public function styles(Worksheet $sheet)
    {
        $styleArray = [
            'font' => [
                'bold' => true,
                'color' => ['rgb' => 'FFFFFF'],
            ],

            'fill' => [
                'fillType' => Fill::FILL_SOLID,
                'startColor' => [
                    'argb' => '33425E',
                ],
            ],
        ];
        return [
            // Style the first row as bold text.
            1 =>$styleArray,
        ];
    }
}
