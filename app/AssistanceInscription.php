<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AssistanceInscription extends Model
{
    const ACTIVE = '1';
    const LOCKED = '0';

    protected $table= 'assistance_inscription';
    protected $fillable=['assistance_id','inscription_id','state','user_created','user_modified'];

    public static function validate(Assistance $assistance,$id)
    {
        return  AssistanceInscription::where(['assistance_id'=>$assistance->id,'inscription_id'=>$id])->first();

    }
}
