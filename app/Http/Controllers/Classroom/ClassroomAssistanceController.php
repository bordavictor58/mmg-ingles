<?php

namespace App\Http\Controllers\Classroom;

use App\Assistance;
use App\Classroom;
use App\Http\Controllers\Controller;
use App\Http\Requests\Assistance\ClassroomAssistanceRequest;
use App\Http\Requests\Assistance\CreateAssistanceRequest;
use App\Http\Requests\Assistance\UpdateAssistanceRequest;
use App\Inscription;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ClassroomAssistanceController extends Controller
{
    function __construct() {
        $this->middleware('auth');
    }

    public function index(Classroom $classroom)
    {
        $classroom->loadCount('scheduledParticipants')
            ->load(['scheduledParticipants']);

        return view('participants.assistances.show', compact('classroom'));
    }

    public function create(Classroom $classroom)
    {
        $assistance = new Assistance();
        return view('participants.assistances.create', compact('assistance', 'classroom'));
    }

    public function store(CreateAssistanceRequest $request, Classroom $classroom)
    {
        $fields = $request->validated();
        $fields['classroom_id'] = $classroom->id;
        $fields['assistance_date']=Carbon::parse($fields['assistance_date'])->format('d-m-Y');
        $meeting = Assistance::create($fields);

        $message = 'La reunión fue registrada correctamente';

        return redirect()->route('classrooms.assistances.index', $classroom->id)->with('success', $message);
    }

    public function show(Classroom $classroom, Assistance $assistance)
    {
        $classroom->loadCount('scheduledParticipants')
            ->load(['scheduledParticipants']);
//        $i = Inscription::find(11);
//        dd($i->assistances);
//        dd($classroom->scheduledParticipants->pivot, $i->assistances);
        return view('participants.assistances.show', ['classroom' => $classroom, 'assistance' => $assistance]);
    }

    public function edit(Classroom $classroom, Assistance $assistance)
    {
        return view('participants.assistances.edit', ['classroom' => $classroom, 'assistance' => $assistance]);
    }

    public function update(UpdateAssistanceRequest $request, Classroom $classroom, Assistance $assistance)
    {
        $fields = $request->validated();
        $fields['classroom_id'] = $classroom->id;
        $assistance->update($fields);

        $message = 'La asistencia fue actualizada correctamente';

        return redirect()->route('classrooms.assistances.index', $classroom->id)->with('success', $message);
    }

    public function delete(ClassroomAssistanceRequest $request, Classroom $classroom) {
        $fields = $request->validated();

        $participants_relation = $fields['assistance'];

        foreach ($participants_relation as $inscription_id) {
            $classroom->inscriptions()->where('id',$inscription_id)->update(['assistance'=>'F']);
        }

        return back();
    }

    public function register(ClassroomAssistanceRequest $request, Classroom $classroom) {
        $fields = $request->validated();

        $participants_relation = $fields['assistance'];

        foreach ($participants_relation as $inscription_id) {
            $classroom->inscriptions()->where('id',$inscription_id)->update(['assistance'=>'A']);
        }

        return back();
    }
}
