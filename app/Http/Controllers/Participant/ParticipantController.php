<?php

namespace App\Http\Controllers\Participant;

use App\Bank;
use App\Category;
use App\Test;
use App\Type;
use App\User;
use DateTime;
use App\Course;
use App\Content;
use App\TestUser;
use App\Classroom;
use App\Inscription;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\Course\CreateCourseTestRequest;
use App\LoginDaily;
use PhpParser\Builder\Class_;

class ParticipantController extends Controller
{
    function __construct(){
        $this->middleware('auth');
        $this->middleware('permission:participant-list-course')->only('list');
        $this->middleware('permission:participant-task-course',['only'=>['document','test']]);
        $this->middleware('permission:participant-register-course',['only'=>['detail','encuesta','register']]);
        $this->middleware('permission:participant-result-course',['only'=>['note','result','certificates']]);
    }

    public function index()
    {
        //
    }


    public function create()
    {
        //
    }


    public function store(Request $request)
    {
        //
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        //
    }


    public function update(Request $request, $id)
    {
        //
    }


    public function destroy($id)
    {
        //
    }
    public function listCategory()
    {
        $categories=Category::all();

        return view('participants.listCategory',compact('categories'));
    }
    public function list(Category $category)
    {
        $date=Carbon::now()->format('d-m-Y');
        $user=User::where('id',Auth::id())->firstOrFail();
        $user_log=User::where('id',Auth::id())->firstOrFail();

        $user=$user->load(['inscriptions'=>function($q) use($category){
            $q->with(['classroom'=> function($query){
                $query->orderBy('created_at','asc');
            }])
                ->whereHas('classroom.course',function($q) use($category){
                $q->where('category_id',$category->id)
                ->where('subcategory_id',null);
            });
        }]);

        $user_log=$user_log->load(['inscriptions'=>function($q) use($category){
            $q->with(['classroom'=> function($query){
                $query->orderBy('created_at','asc');
            }])->whereHas('classroom.course',function($q) use($category){
                $q->where('category_id',$category->id)
                ->where('subcategory_id','!=',null);
            });
        }]);


        return view('participants.list',compact('user','user_log','date'));
    }

    public function document(User $user,Classroom $classroom= null)
    {
        if($classroom){
            $content=Content::where(['course_id'=>$classroom->course->id,'type_id' => 2])
            ->with('media')->get();
        }else{
            $inscriptionInducc=Inscription::where(['classroom_id'=> 2,'user_id'=>$user->id])->whereColumn('grade','>=','grade_min')->first();
            $inscriptionTea=Inscription::where(['classroom_id'=> 17,'user_id'=>$user->id])->whereColumn('grade','>=','grade_min')->first();
            $content=Content::where(['type_id' => 2])
            ->with('media')->get();
        }

        return view('participants.document',compact('content','classroom','inscriptionInducc','inscriptionTea'));
    }
    public function saycheese(Classroom $classroom )
    {
        $inscription=Inscription::where(['classroom_id'=>$classroom->id,'user_id'=> Auth::id()])->firstOrFail();

        return view('participants.sayCheese',compact('classroom','inscription'));
    }
    public function detail(Classroom $classroom )
    {
        $user=Auth::user();
        $content=Content::where(['course_id'=>$classroom->course->id,'type_id' => 5])->first();


        $inscription=Inscription::where(['classroom_id'=>$classroom->id,'user_id'=>$user->id])->firstOrFail();


        $Isnull  = Inscription::where('id',$inscription->id)->select('first_access')->first()->first_access;
            $IsInscription = LoginDaily::where(['user_id' => Auth::id() , 'inscription_id' => $inscription->id])->get();

            if(is_null($Isnull))
            {
                $inscription = Inscription::find($inscription->id);
                $inscription->first_access = date('Y-m-d H:i:s');
                $inscription->save();
            }

            if(count($IsInscription)>0)
            {
                $daily = LoginDaily::find($IsInscription[0]->id);
                $daily->user_id = Auth::id();
                $daily->inscription_id = $inscription->id;
                $daily->count = $daily->count + 1;
                $daily->state = 1;
                $daily->save();
            }else{
                $daily = new LoginDaily;
                $daily->user_id = Auth::id();
                $daily->inscription_id = $inscription->id;
                $daily->count = 1;
                $daily->state = 1;
                $daily->save();
            }

            if($content==null){
                return redirect()->route('participant.course.test', [$classroom,$classroom->tests->first()]);
            }
        return view('participants.detail',compact('content','classroom','user','inscription'));
    }

    public function test(Classroom $classroom, Test $test)
    {
        if($classroom->course->id==9) return redirect()->route('participant.course.note.nofound',$classroom);
        $user=Auth::user();
        $date = Carbon::now()->format('Y-m-d H:i:s');
        $inscription=Inscription::where(['classroom_id'=>$classroom->id,'user_id'=>$user->id])->firstOrFail();
        $Isnull  = Inscription::where('id',$inscription->id)->select('first_access')->first()->first_access;
            $IsInscription = LoginDaily::where(['user_id' => Auth::id() , 'inscription_id' => $inscription->id])->get();

            if(is_null($Isnull))
            {
                $inscription = Inscription::find($inscription->id);
                $inscription->first_access = date('Y-m-d H:i:s');
                $inscription->save();
            }

            if(count($IsInscription)>0)
            {
                $daily = LoginDaily::find($IsInscription[0]->id);
                $daily->user_id = Auth::id();
                $daily->inscription_id = $inscription->id;
                $daily->count = $daily->count + 1;
                $daily->state = 1;
                $daily->save();
            }else{
                $daily = new LoginDaily;
                $daily->user_id = Auth::id();
                $daily->inscription_id = $inscription->id;
                $daily->count = 1;
                $daily->state = 1;
                $daily->save();
            }
            $inscription->update(['assistance' => "A"]);
            if($classroom->course->no_test == 1){

                return view('participants.participo',compact('inscription','classroom'));
            }

            $classroom->load(['course.banks' => function($query) use($test) {
                if($test->random == 1){
                    $query->inRandomOrder()->limit($test->number_question)->where('banks.parent_id',null)->where('banks.state','!=',0)->with(['childs']);
                }else{
                    $query->limit($test->number_question)->where('banks.parent_id',null)->where('banks.state','!=',0)->with(['childs']);
                }
            }])->get();

        return view('participants.test', compact('classroom','test','date','inscription'));
    }
    public function register(CreateCourseTestRequest $request,Classroom $classroom,Test $test)
    {
        #recives solo el array de examen
        $inputs = $request->validated(); ### tienes q recivir el tiemp que demor en dar el examen
        $date_start= new Datetime($inputs['date']);
        $date_end = new DateTime(Carbon::now()->format('Y-m-d H:i:s'));

        $hour_calculated = date_diff($date_start,$date_end);
        $hour=$hour_calculated->format("%H:%I:%S");

        $questions = [];
        $answers = [];
        # usaurio que dio el examen
        $user = Auth::user();
        # recorremos el arreglo para poder divridirlos en arrays distitnos
        foreach($inputs['inputs'] as $key => $value) {
            array_push($questions, $key);
            array_push($answers, $value);
        }
        # recupperamos las respuestas correctas y alamacenamos en un array
        $correct_answers= Bank::with(['parent' => function($query) use($questions){
            $query->whereIn('id',$questions);
        }])
        ->whereHas('parent',function($query) use($questions){
            $query->whereIn('id',$questions);
        })
        ->where('is_correct',1)
        ->pluck('id')->toArray();

        # calculamos el nro de respuestas correctas del participante
        $number_correct_answer = count(array_intersect($answers,$correct_answers));

        # calculamos el valor de cada pregunta
        $question_value = $test->grade_max / $test->number_question;

        # calculamos la nota de partcipante
        $grade = round($question_value *$number_correct_answer);

        $tried = TestUser::where([
            'user_id' => $user->id,
            'test_id' => $test->id
            ])->count();

        # almacenamos el regitros de test user
        $test_user = TestUser::create([

            'user_id' => $user->id,
            'test_id' => $test->id,
            'questions' => $questions,
            'answers' => $answers,
            'correct_answers' => $correct_answers,
            'tried' => $tried+1,
            'time' => $hour,
            'grade' => $grade
        ]);

        return redirect(route('participant.course.note',[$classroom,$test_user]));
    }
    public function noteNofound(Classroom $classroom){

        return view('participants.aproboNoFound',compact('classroom'));
    }
    public function note(Classroom $classroom,TestUser $test_user=null)
    {
        $inscription =Inscription::where(['user_id' => $test_user->user_id , 'classroom_id' => $classroom->id])
            ->active()
            ->first();
        $classroom2=null;
        if($classroom->course->subcategory_id != null){
            $classroom2=Classroom::with('course')
            ->whereHas('course', function($q) use($classroom){
                $q->where('subcategory_id',$classroom->course->subcategory_id);
            })->where('classrooms.id','>',$classroom->id)
            ->first();
        }
        if($test_user->grade < $inscription->grade_min )
        {

                return view('participants.recuperacion',compact('inscription','test_user','classroom','classroom2'));

        }
        return view('participants.aprobo',compact('inscription','test_user','classroom','classroom2'));

    }

    public function certificates ()
    {
       //32
        $user=Classroom::where('id',32)->first();
        $user->load('inscriptions');


        return view('users.certificate', compact('user'));
    }

    public function result(Classroom $classroom,TestUser $test_user)
    {
        $classroom->load(['course.banks' => function($query) use($test_user) {
            $query->where('banks.parent_id',null)->whereIn('banks.id',$test_user->questions)->with(['childs']);
        }])->get();
        //determinarmos las correctas e incorrectas

        $incorrect_answers=array_diff($test_user->answers,$test_user->correct_answers);
        $correct_answers = array_diff($test_user->answers,$incorrect_answers);

        return view('tests.detail',compact('test_user','classroom','incorrect_answers','correct_answers'));
    }

    public function encuesta(Classroom $classroom,TestUser $test_user)
    {
        $classroom->load('course');

        return view('participants.encuesta',compact('classroom','test_user'));
    }

}
