<?php

namespace App\Http\Controllers\Participant;

use App\Course;
use App\Classroom;
use Carbon\Carbon;
use App\Inscription;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CourseController extends Controller
{
    public function index()
    {
        $date_actual = Carbon::now()->format('Y-m-d');

        $classrooms=Classroom::with('course','facilitator')
        ->with(['facilitator', 'course'])
        ->whereDate('start_datetime', '<=', $date_actual)
        ->whereDate('end_datetime', '>=', $date_actual)
        ->get();
        return view('courses.show', compact('classrooms'));

    }
    public function getDetail(Classroom $classroom){


        $classroom->load(['participants'=>function($q){
            $q->whereNotIn('users.id',[1,2,5,7,5762,5763]);
        }]);

        return view('participants.detail_course', compact('classroom'));
    }
}
