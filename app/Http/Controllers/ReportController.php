<?php

namespace App\Http\Controllers;
use DB;
use App\Course;
use App\Exports\Participant\ReportAssistance;
use App\Exports\Participant\ReportGeneral;
use App\Exports\ReportHistoryGeneral;
use App\Inscription;
use Illuminate\Http\Request;
use App\Exports\ReportsExport;
use App\Exports\ReportDetailGrade;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\ReportDetailGradeBegin;
use App\Http\Requests\ReportContrataRequest;

class ReportController extends Controller
{
    public function index()
    {
        $courses = Course::getActive();
        return view('reports.general',compact('courses'));

    }
    public function exportExcel(Request $request)
    {
        ini_set('max_execution_time', 720000);
        ini_set('memory_limit', -1);
        // $file= public_path(). "/files/consolidado.xlsx";

        $start = $request->input('start_date');
        $end = $request->input('end_date');
        $course_id = $request->input('course_id');


        $export = new ReportsExport($start, $end, $course_id);
        return Excel::download($export, 'reporte.xlsx');

    }

    public function exportDetailGrade()
    {
        ini_set('max_execution_time', 720000);
        ini_set('memory_limit', -1);

        $export = new ReportDetailGrade;
        return Excel::download($export, 'reporte.xlsx');
    }
    public function exportDetailGradeBegin()
    {
        ini_set('max_execution_time', 720000);
        ini_set('memory_limit', -1);

        $export = new ReportDetailGradeBegin;
        return Excel::download($export, 'reporte.xlsx');
    }
    public function reportGeneral()
    {
        $courses=Course::active()->get();

        return view('reports.participant.general',compact('courses'));
    }

    public function reportAssistance(ReportContrataRequest $request)
    {

        ini_set('max_execution_time', 720000);
        ini_set('memory_limit', -1);

        $start = $request->input('start_date');
        $end = $request->input('end_date');
        $course_id = $request->input('course_id');

        $export = new ReportAssistance($start, $end, $course_id);
        return Excel::download($export, 'reporte.xlsx');

    }

    public function reportHistoryGeneral(ReportContrataRequest $request)
    {
        ini_set('max_execution_time', 720000);
        ini_set('memory_limit', -1);

        $start = $request->input('start_date');
        $end = $request->input('end_date');
        $course_id = $request->input('course_id');

        $export = new ReportHistoryGeneral($start, $end, $course_id);
        return Excel::download($export, 'reporte.xlsx');

    }

    public function ReportAcademygeneral(ReportContrataRequest $request)
    {
        ini_set('max_execution_time', 720000);
        ini_set('memory_limit', -1);

        $start = $request->input('start_date');
        $end = $request->input('end_date');
        $course_id = $request->input('course_id');

        $export = new ReportGeneral($start, $end, $course_id);
        return Excel::download($export, 'reporteGeneral.xlsx');
    }
}
