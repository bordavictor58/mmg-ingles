<?php

namespace App\Http\Requests\Participant;

use App\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class CreateParticipantRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    public function rules() : array
    {
        $rules = [];

        $rules=[
            'type_doc'=> 'required|in:1,2,3',
            'management' => 'nullable',
            'name' => ['required', 'string', 'max:70'],
            'last_name' => ['required', 'string', 'max:90'],
            'area' => ['present', 'max:70'],
            'position' => ['present', 'max:70'],
            'email' => [
                'required','email', 'max:255',
                Rule::unique('users')->ignore($this->id)
            ],
            'phone' => ['present', 'max:20'],
            'avatar'=>'mimes:jpeg,png,jpg,JPEG,PNG,JPG',
            'password' => ['confirmed'],
            'proyecto' => 'nullable'
            //'password' => ['required', 'string', 'min:8', 'confirmed'],
        ];
        if($this->type_doc == User::DNI){
            $rules['dni'] = [
            'required', 'alpha_num', 'min:8', 'max:8',
            Rule::unique('users')->ignore($this->id)
            ];
        }
        if($this->type_doc == User::PASAPORTE || $this->type_doc == User::CARNET_EXT){
            $rules['dni'] = [
            'required', 'alpha_num', 'min:12', 'max:12',
            Rule::unique('users')->ignore($this->id)
            ];
        }
        return $rules;
    }
    public function messages()
    {
        return [

            'avatar.mimes'=>'Formato de la imagen no aceptada',

        ];
    }
    public function attributes()
{
    return[
        'dni' => 'documento',
    ];

}


}
