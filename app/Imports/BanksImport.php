<?php

namespace App\Imports;

use App\Bank;
use App\Test;
use App\Content;
use App\Course;
use Exception;
use Illuminate\Validation\Rule;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;

class BanksImport implements ToCollection, WithHeadingRow, WithValidation
{
    protected $course;

    public function __construct(Course $course)
    {
        $this->course=$course;
    }

    public function rules(): array
    {
        return [
            // Siempre valida por lotes
            // Fila.columna
            '*.pregunta' =>  'required',
            '*.tipo' => Rule::in(['INICIO', 'REGULAR']),
            '*.tipo' => 'nullable',
            '*.correcto' => 'nullable'
        ];
    }
    public function customValidationMessages()
    {
        return[
            'pregunta.required'=>'La pregunta es requerida',
        ];
    }
    public function collection(Collection $rows)
    {

        Bank::where(['content_id' => $this->course->content->id,'state'=>1])->update(['state'=>0]);

        $all_questions = $rows->filter(function ($row, $key){

            if($row['tipo']!= null){
                $bank_question=Bank::create([
                    'title' => (string)trim($row['pregunta']),
                    'is_correct' => 0,
                    'is_question' => 1,
                    'parent_id' => null,
                    'content_id' => $this->course->content->id,
                    'state'=>1
                ]);
                return  $row['codigo'] = $bank_question->id;
            }
        });
        $correcto=0;
        $all_answers = $rows->filter(function ($row_answer, $key_answer) use($all_questions,$correcto){

            if($row_answer['correcto'] != null){
                $correcto = 1;
            }
            if($row_answer['pregunta'] === true){
                $row_answer['pregunta'] ="VERDADERO";
            }
            if($row_answer['pregunta'] === false){
                $row_answer['pregunta'] ="FALSO";
            }
            $array_total_keys=array_keys($all_questions->toArray());

            if($row_answer['tipo'] == null){


                $solution =$all_questions->filter(function ($row_question,$key_question) use($row_answer,$key_answer,$correcto,$all_questions,$array_total_keys){

                    if($key_answer > $key_question){
                        $row_answer['parent_id'] = $row_question['codigo'];
                    }
                });
                $bank_answer=Bank::create([
                    'title' => (string)trim($row_answer['pregunta']),
                    'is_correct' => $correcto,
                    'is_question' => 0,
                    'parent_id' => $row_answer['parent_id'],
                    'content_id' => $this->course->content->id,
                    'state'=>1
                ]);
                return $row['codigo_answer'] = $bank_answer->id;
            }
        });

        return $all_answers->all();
    }

}
