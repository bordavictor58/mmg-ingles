<?php

namespace App\Imports;

use App\User;
use App\Classroom;
use App\Inscription;
use Carbon\Carbon;
use DateTime;
use Exception;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class InscriptionImport implements WithHeadingRow,ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */

    public function model(array $row)
    {
        $el = array_filter($row);
        if($el==[]){
            return null;

        }
        $contrata = Auth::user();
        $classroom = Classroom::get()->modelKeys();
        $grade_tried=[];
        $grade_date=[];

        try{
            $user = User::where('dni',trim($row['dni']))->firstOrFail();
        }catch(Exception $e){

            throw new Exception("El participante con el DNI: ".trim($row['dni'])." No existe primero registrelo y luego inscriba");
        }

        try{
            $classrooms=Classroom::where('id',$row['programacion'])->firstOrFail();
        }catch(Exception $e){

            throw new Exception("La programación con el ID: ".trim($row['programacion'])." No existe verifique que sea la correcta");
        }



        $inscription=Inscription::updateOrCreate(
            ['user_id'=>$user->id,'classroom_id'=>$row['programacion']],
            [
                'user_created'=>$contrata->id,
                'grade_min' => $classrooms->course->grade_min,
                'type' => $row['tipo'],
            ]);
            $classroom_test=$inscription->classroom;
        if($classroom_test->course->subcategory_id != null){
            $clFor=Classroom::where(['course_id'=>$classroom_test->course->id,'subcategory_id'=>$classroom->course->subcategory_id])->where('id','!=',$classroom->id)->active()->get();
            foreach($clFor as $cl){
                Inscription::updateOrCreate(
                    ['user_id'=>$user->id,'classroom_id'=>$cl->id],
                    [
                        'user_created'=>$contrata->id,
                        'grade_min' => $cl->course->grade_min,
                        'type' => 'REGULAR',
                    ]);
            }
        }
    }
}
