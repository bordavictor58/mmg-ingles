<?php

namespace App\Imports;

use App\Company;
use App\Gerency;
use App\User;
use Auth;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\SkipsErrors;
use Maatwebsite\Excel\Concerns\SkipsOnError;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Throwable;
use Illuminate\Support\Facades\Validator;

class ParticipantImport implements ToModel, WithHeadingRow, SkipsOnError
{

    use Importable, SkipsErrors;
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */



    public function model(array $row)
    {
        $el = array_filter($row);
        if($el==[]){
            return null;

        }
        Validator::make($row,[
                'ruc' => 'required|max:11|min:11'
            ],['ruc.required'=>'El ruc es requerido',
            'ruc.min'=>'El ruc debe tener 11 digitos',
            'ruc.max'=>'El ruc debe tener 11 digitos'])->validate();

        if(in_array($row['tipodocumento'],['ID','id'])){
            $type_doc=User::ID;
            Validator::make($row,[
            'documento' => 'required|max:8|min:6',
            ],['documento.max' => 'El documento no debe ser mayor que 8 caracteres: '.$row['documento'],
                'documento.min' => 'El documento debe ser igual o mayor que 6 caracteres: '.$row['documento']])->validate();
        }
        elseif(in_array($row['tipodocumento'],['DNI','dni'])){
            $type_doc=User::DNI;
            Validator::make($row,[
            'documento' => 'required|max:8|min:8',
            ],['documento.max' => 'El documento no debe ser mayor que 8 caracteres: '.$row['documento'],
                'documento.min' => 'El documento debe ser igual que 8 caracteres: '.$row['documento']])->validate();}
        elseif(in_array($row['tipodocumento'],['CE','ce','PASAPORTE','pasaporte'])){
            if(in_array(trim($row['tipodocumento']),['CE','ce'])) $type_doc=User::CARNET_EXT;
            if(in_array(trim($row['tipodocumento']),['PASAPORTE','pasaporte'])) $type_doc=User::PASAPORTE;

            Validator::make($row,[
                'documento' => 'required|max:12|min:12|alpha_num',
            ],['documento.max' => 'El documento no debe ser mayor que 12 caracteres: '.$row['documento'],
            'documento.min' => 'El documento debe ser igual que 12 caracteres: '.$row['documento']])->validate();
            }else{Validator::make($row,[
                'tipodocumento' => 'required|max:9|min:2|in:DNI,dni,CE,ce,PASAPORTE,pasaporte'
            ])->validate();

        }


        $company = Company::where('ruc', trim($row['ruc']))->firstOrFail();

        $participant = User::where('dni',trim($row['documento']))->role('participante')->first();


        $contrata =Auth::user();


        if($participant)
        {
            $participant->update([
                'type_doc'=>$type_doc,
                'dni' => $participant->dni,
                'management' => $row['administracion'],
                'last_name' => $row['apellido_paterno']." ".$row['apellido_materno'],
                'name' => $participant->name,
                'area' => $row['area'],
                'position' => $row['puesto'],
                'proyecto' => $row['proyecto'],
                'management' => $row['administracion'],
                'company_id' => $company->id,
                'email' => $row['email'],
                'phone' => $row['telefono'],
                'user_created' => $contrata,
                'user_modified' => $contrata,
                'password'=>bcrypt($row['documento'])
            ]);
        }else{
            $participant = User::where('dni',trim($row['documento']))->first();
            if($participant){
                return null;
            }
            $participant=User::create([
                'type_doc'=>$type_doc,
                'dni' => $row['documento'],
                'management' => $row['administracion'],
                'last_name' => $row['apellido_paterno']." ".$row['apellido_materno'],
                'name' => $row['nombres'],
                'area' => $row['area'],
                'position' => $row['puesto'],
                'proyecto' => $row['proyecto'],
                'company_id' => $company->id,
                'email' =>$row['email'],
                'phone' => $row['telefono'],
                'user_created' => $contrata,
                'user_modified' => $contrata,
                'password'=>bcrypt($row['documento'])
            ]);
        }
        $participant->assignRole('participante');

    }
    public function onError(Throwable $error)
    {

    }
}
