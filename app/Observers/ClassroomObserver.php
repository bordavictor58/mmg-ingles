<?php

namespace App\Observers;

use App\Classroom;
use App\Course;
use App\Inscription;
use App\Test;
use Illuminate\Support\Facades\Date;

class ClassroomObserver
{
    /**
     * Handle the classroom "created" event.
     *
     * @param  \App\Classroom  $classroom
     * @return void
     */
    public function created(Classroom $classroom)
    {
        Inscription::create(['user_id'=>2,'classroom_id'=>$classroom->id,'grade_min'=> $classroom->grade_min,'type'=>'REGULAR']);
        Test::create(['classroom_id'=>$classroom->id,'name'=>'FINAL EXAM','random'=>1,'time'=>'00:10:00','tried'=>2,
                    'start_date'=>Date::now(),'end_date'=>Date::now(),'number_question'=>10]);
        // if($classroom->course->subcategory_id != null){
        //     $courses=Course::where(['subcategory_id'=>$classroom->course->subcategory_id])->active()->get();
        //     $course_count=count($courses);
        //     if($course_count>1){
        //         // foreach($courses as $course){

        //         //     $classroomObj=Classroom::where(['course_id',$course->id])->whereRaw('DATE_FORMAT(created_at)')->active()->get();
        //         // }
        //     }

        // }
    }

    /**
     * Handle the classroom "updated" event.
     *
     * @param  \App\Classroom  $classroom
     * @return void
     */
    public function updated(Classroom $classroom)
    {

        //
    }

    /**
     * Handle the classroom "deleted" event.
     *
     * @param  \App\Classroom  $classroom
     * @return void
     */
    public function deleted(Classroom $classroom)
    {
        //
    }

    /**
     * Handle the classroom "restored" event.
     *
     * @param  \App\Classroom  $classroom
     * @return void
     */
    public function restored(Classroom $classroom)
    {
        //
    }

    /**
     * Handle the classroom "force deleted" event.
     *
     * @param  \App\Classroom  $classroom
     * @return void
     */
    public function forceDeleted(Classroom $classroom)
    {
        //
    }
}
