<?php

use App\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = User::create([
            'company_id' => 1,
            'dni' => '72974767',
            'name'=>'Administrador',
            'last_name'=>'igh',
            'email'=>'administradoe@mail.com',
            'password'=> bcrypt('root'),
        ]);
        $participant = User::create([
            'company_id' => 1,
            'dni' => '46185127',
            'name'=>'Participante',
            'last_name'=>'igh',
            'email'=>'participant@ighgroup.com',
            'password'=> bcrypt('root'),
        ]);
        $contrata = User::create([
            'company_id' => 1,
            'dni' => '11221122',
            'name'=>'contrata',
            'last_name'=>'igh',
            'email'=>'contrata@ighgroup.com',
            'password'=> bcrypt('root'),
        ]);
        $facilitator = User::create([
            'company_id' => 1,
            'dni' => '11221133',
            'name'=>'Facilitador',
            'last_name'=>'igh',
            'email'=>'facilitador@ighgroup.com',
            'password'=> bcrypt('11221133'),
        ]);

        $admin_permissions = Permission::pluck('id', 'id')->all();

        $participant_permissions = Permission::where('permissions.name','=', 'course-mine')
        ->orWhere('permissions.name','like', '%' .'certificate-mine'. '%')->pluck('id', 'id');

        $contrata_permissions = Permission::where('permissions.name','like', '%' .'inscription'. '%')
        ->orWhere('permissions.name','like', '%' .'participant'. '%')
        ->orWhere('permissions.name', '=', 'classroom-list')->pluck('id', 'id');

        $facilitator_permissions = Permission::where('permissions.name','like', '%' .'course'. '%')
        ->orWhere('permissions.name','=', 'classroom-list')
        ->orWhere('permissions.name','=', 'classroom-consolidated')
        ->pluck('id', 'id');

        //admin
        $role = Role::findByName('administrador');
        $role->syncPermissions($admin_permissions);
        $role->revokePermissionTo('course-mine');
        $role->revokePermissionTo('classroom-mine');
        $role->revokePermissionTo('inscription-mine');
        $role->revokePermissionTo('inscription-assistance');
        $admin->assignRole([$role->id]);
        //$admin->companies()->attach('1');

        // contratistas
        $rol = Role::findByName('contratista');
        $rol->syncPermissions($contrata_permissions);
        $contrata->assignRole([$rol->id]);

        //$contrata->companies()->attach('1');

        // participantes
        $roles = Role::findByName('participante');
        $roles->syncPermissions($participant_permissions);
        $roles->givePermissionTo('test-list');
        $roles->givePermissionTo('course-test-mine');
        $participant->assignRole([$roles->id]);
        //$participant->companies()->attach('1');

        // facilitador
        $rols= Role::findByName('facilitador');
        $rols->syncPermissions($facilitator_permissions);
        $rols->revokePermissionTo('course-mine');
        $rols->givePermissionTo('inscription-assistance');
        //$facilitators->companies()->attach('1');
        $facilitator->assignRole([$rols->id]);
        //$facilitator->companies()->attach('1');


    }
}
