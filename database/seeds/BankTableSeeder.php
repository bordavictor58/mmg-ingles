<?php

use Illuminate\Database\Seeder;
use App\Bank;

class BankTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //NOTIFICACION, INVESTIGACION Y REPORTE DE INCIDENTES PELIGROSOS Y ACCIDENTES DE TRABAJO
        //1
        Bank::create([
            'title'=>'Uno de los objetivos del Sistema de Gestión de Seguridad es:',
            'is_question'=> 1,
            'content_id'=> 1,
        ]);
        Bank::create([
            'title'=>'Mejorar los resultados que se muestran en la seguridad y salud de nuestros trabajadores.',
            'is_question'=> 0,
            'parent_id'=> 1,
            'content_id'=> 1,
        ]);
        Bank::create([
            'title'=>'Alcanzar un nivel de 92% en la Implementación de los Estándares del Sistema de Gestión en el año 2021. ',
            'is_question'=> 0,
            'is_correct'=> 1,
            'parent_id'=> 1,
            'content_id'=> 1,
        ]);
        Bank::create([
            'title'=>'Dar a conocer el Reglamento de Constitución y Funcionamiento del Comité de SST.',
            'is_question'=> 0,
            'parent_id'=> 1,
            'content_id'=> 1,
        ]);
        Bank::create([
            'title'=>'Socializar el Plan para la vigilancia, prevención y control de COVID-19',
            'is_question'=> 0,
            'parent_id'=> 1,
            'content_id'=> 1,
        ]);
        //PREGUNTA2-6
        Bank::create([
            'title'=>'Cuáles son nuestros valores corporativos:',
            'is_question'=> 1,
            'content_id'=> 1,
        ]);
        Bank::create([
            'title'=>'Seguridad, excelencia, compromiso, confianza, responsabilidad e integridad.',
            'is_question'=> 0,
            'is_correct' => 1,
            'parent_id'=> 6,
            'content_id'=> 1,
        ]);
        Bank::create([
            'title'=>'Seguridad, excelencia, solidaridad, confianza, responsabilidad e integridad.',
            'is_question'=> 0,
            'parent_id'=> 6,
            'content_id'=> 1,
        ]);
        Bank::create([
            'title'=>'Seguridad, excelencia, iniciativa, confianza, responsabilidad e integridad.',
            'is_question'=> 0,
            'parent_id'=> 6,
            'content_id'=> 1,
        ]);
        Bank::create([
            'title'=>'Seguridad, excelencia, compromiso, tolerancia, responsabilidad e integridad.',
            'is_question'=> 0,
            'parent_id'=> 6,
            'content_id'=> 1,
        ]);
        //PREGUNTA3-11
        Bank::create([
            'title'=>'Sobre la Importancia del trabajador en el programa de Seguridad y Salud Ocupacional.',
            'is_question'=> 1,
            'content_id'=> 1,
        ]);
        Bank::create([
            'title'=>'Se considera al empleador como el recurso más importante.',
            'is_question'=> 0,
            'parent_id'=> 11,
            'content_id'=> 1,
        ]);
        Bank::create([
            'title'=>'La empresa considera que la seguridad es responsabilidad del Estado.',
            'is_question'=> 0,
            'parent_id'=> 11,
            'content_id'=> 1,
        ]);
        Bank::create([
            'title'=>'La importancia del trabajador en el programa tiene un objetivo preventivo.',
            'is_question'=> 0,
            'is_correct'=>1,
            'parent_id'=> 11,
            'content_id'=> 1,
        ]);
        Bank::create([
            'title'=>'La importancia del trabajador en el programa tiene un objetivo informativo.',
            'is_question'=> 0,
            'parent_id'=> 11,
            'content_id'=> 1,
        ]);
        //PREGUNTA4-16
        Bank::create([
            'title'=>'Quiénes tienen participación en el programa de SSO.',
            'is_question'=> 1,
            'content_id'=> 1,
        ]);
        Bank::create([
            'title'=>'Trabajadores, empleadores y estado.',
            'is_question'=> 0,
            'is_correct'=> 1,
            'parent_id'=> 16,
            'content_id'=> 1,
        ]);
        Bank::create([
            'title'=>'Empleadores, directivos y estado.',
            'is_question'=> 0,
            'parent_id'=> 16,
            'content_id'=> 1,
        ]);
        Bank::create([
            'title'=>'Empleadores, Trabajadores y municipalidades.',
            'is_question'=> 0,
            'parent_id'=> 16,
            'content_id'=> 1,
        ]);
        Bank::create([
            'title'=>'Trabajadores, empleadores y sindicato.',
            'is_question'=> 0,
            'parent_id'=> 16,
            'content_id'=> 1,
        ]);
        //PREGUNTA5-21
        Bank::create([
            'title'=>'Cuál es el Reglamento que norma la prevención de incidentes, incidentes peligrosos, accidentes de trabajo y enfermedades ocupacionales.',
            'is_question'=> 1,
            'content_id'=> 1,
        ]);
        Bank::create([
            'title'=>'Reglamento de Inspecciones Técnicas de seguridad en Defensa Civil, y sus modificatorias D.S. N° 100-2003-PC, D.S. N° 074 – 2005 – PCM. ',
            'is_question'=> 0,
            'parent_id'=> 21,
            'content_id'=> 1,
        ]);
        Bank::create([
            'title'=>'Reglamento de Seguridad y Salud Ocupacional en Minería D.S. No. 024-2016-EM',
            'is_question'=> 0,
            'is_correct'=>1,
            'parent_id'=> 21,
            'content_id'=> 1,
        ]);
        Bank::create([
            'title'=>'Reglamento de Constitución de Comité y nombramiento de supervisor de SSO.',
            'is_question'=> 0,
            'parent_id'=> 21,
            'content_id'=> 1,
        ]);
        Bank::create([
            'title'=>'Ley 29783, Ley de Seguridad y Salud en el Trabajo. ',
            'is_question'=> 0,
            'parent_id'=> 21,
            'content_id'=> 1,
        ]);
         //PREGUNTA6-26
        Bank::create([
            'title'=>'Cuál de estos reglamentos no forma parte del Reglamento Interno:',
            'is_question'=> 1,
            'content_id'=> 1,
        ]);
        Bank::create([
            'title'=>'Reglamento Interno de Trabajo.',
            'is_question'=> 0,
            'parent_id'=> 26,
            'content_id'=> 1,
        ]);
        Bank::create([
            'title'=>'Reglamento Interno de Higiene',
            'is_question'=> 0,
            'is_correct'=>1,
            'parent_id'=> 26,
            'content_id'=> 1,
        ]);
        Bank::create([
            'title'=>'Reglamento Interno de Seguridad y Salud en el Trabajo.',
            'is_question'=> 0,
            'parent_id'=> 26,
            'content_id'=> 1,
        ]);
        Bank::create([
            'title'=>'Reglamento Interno de Tránsito.',
            'is_question'=> 0,
            'parent_id'=> 26,
            'content_id'=> 1,
        ]);
        //PREGUNTA7-31
        Bank::create([
            'title'=>'Con respecto al Plan para la Vigilancia, prevención y control de COVID-19 es incorrecto:',
            'is_question'=> 1,
            'content_id'=> 1,
        ]);
        Bank::create([
            'title'=>'Mantener el distanciamiento físico.',
            'is_question'=> 0,
            'parent_id'=> 31,
            'content_id'=> 1,
        ]);
        Bank::create([
            'title'=>'No usar de la bitácora de contactos.',
            'is_question'=> 0,
            'is_correct'=> 1,
            'parent_id'=> 31,
            'content_id'=> 1,
        ]);
        Bank::create([
            'title'=>'Utilizar mascarillas',
            'is_question'=> 0,
            'parent_id'=> 31,
            'content_id'=> 1,
        ]);
        Bank::create([
            'title'=>'Lavarnos las manos',
            'is_question'=> 0,
            'parent_id'=> 31,
            'content_id'=> 1,
        ]);

        //PREGUNTA8-36
        Bank::create([
            'title'=>'El distanciamiento mínimo establecido para la prevención de contagio es:',
            'is_question'=> 1,
            'content_id'=> 1,
        ]);
        Bank::create([
            'title'=>'1.0 m. de distancia con tus compañeros.',
            'is_question'=> 0,
            'parent_id'=> 36,
            'content_id'=> 1,
        ]);
        Bank::create([
            'title'=>'1.5 m de distancia con tus compañeros.',
            'is_question'=> 0,
            'parent_id'=> 36,
            'is_correct'=> 1,
            'content_id'=> 1,
        ]);
        Bank::create([
            'title'=>'2.0 m. de distancia con tus compañeros.',
            'is_question'=> 0,
            'parent_id'=> 36,
            'content_id'=> 1,
        ]);
        Bank::create([
            'title'=>'2.5 m. de distancia con tus compañeros.',
            'is_question'=> 0,
            'parent_id'=> 36,
            'content_id'=> 1,
        ]);
        //PREGUNTA9-41
        Bank::create([
            'title'=>'_______________ lidera y brinda los recursos para el desarrollo de todas las actividades para implementar el SSO.',
            'is_question'=> 1,
            'content_id'=> 1,
        ]);
        Bank::create([
            'title'=>'El ingeniero responsable.',
            'is_question'=> 0,
            'parent_id'=> 41,
            'content_id'=> 1,
        ]);
        Bank::create([
            'title'=>'El trabajador',
            'is_question'=> 0,
            'parent_id'=> 41,
            'content_id'=> 1,
        ]);
        Bank::create([
            'title'=>'La Alta Gerencia.',
            'is_question'=> 0,
            'is_correct'=>1,
            'parent_id'=> 41,
            'content_id'=> 1,
        ]);
        Bank::create([
            'title'=>'El sindicato.',
            'is_question'=> 0,
            'parent_id'=> 41,
            'content_id'=> 1,
        ]);
        ///aumentamos
        //PREGUNTA10-46
        Bank::create([
            'title'=>'______________ son responsables del cumplimiento de las políticas de seguridad y salud ocupacional:',
            'is_question'=> 1,
            'content_id'=> 1,
        ]);
        Bank::create([
            'title'=>'El empleador y las empresas contratistas',
            'is_question'=> 0,
            'parent_id'=> 46,
            'content_id'=> 1,
        ]);
        Bank::create([
            'title'=>'Los funcionarios de línea y todos los trabajadores.',
            'is_question'=> 0,
            'is_correct'=>1,
            'parent_id'=> 46,
            'content_id'=> 1,
        ]);
        Bank::create([
            'title'=>'Los jefes y los trabajadores',
            'is_question'=> 0,
            'parent_id'=> 46,
            'content_id'=> 1,
        ]);
        Bank::create([
            'title'=>'El área de seguridad y sus trabajadores.',
            'is_question'=> 0,
            'parent_id'=> 46,
            'content_id'=> 1,
        ]);
        //PREGUNTA11-51
        Bank::create([
            'title'=>'Forman parte de las políticas complementarias de SSO',
            'is_question'=> 1,
            'content_id'=> 1,
        ]);
        Bank::create([
            'title'=>'Política de Ambiente de Trabajo Libre de decir sí',
            'is_question'=> 0,
            'parent_id'=> 51,
            'content_id'=> 1,
        ]);
        Bank::create([
            'title'=>'Política de Ambiente de Trabajo Libre de Alcohol y Drogas',
            'is_question'=> 0,
            'is_correct'=>1,
            'parent_id'=> 51,
            'content_id'=> 1,
        ]);
        Bank::create([
            'title'=>'Política de Uso Responsable mensajes',
            'is_question'=> 0,
            'parent_id'=> 51,
            'content_id'=> 1,
        ]);
        Bank::create([
            'title'=>'Política de Comportamiento de seguridad e higiene.',
            'is_question'=> 0,
            'parent_id'=> 51,
            'content_id'=> 1,
        ]);
        //PREGUNTA12-56
        Bank::create([
            'title'=>'Asegurar el desarrollo de pruebas de detección de alcohol en los conductores de vehículos previo a sus actividades, dentro o fuera de la unidad minera o proyecto. Es parte de la ____________________________.',
            'is_question'=> 1,
            'content_id'=> 1,
        ]);
        Bank::create([
            'title'=>'Política del Derecho a Decir NO.',
            'is_question'=> 0,
            'parent_id'=> 56,
            'content_id'=> 1,
        ]);
        Bank::create([
            'title'=>'Política del Comportamiento Seguro y Cultura de Seguridad.',
            'is_question'=> 0,
            'parent_id'=> 56,
            'content_id'=> 1,
        ]);
        Bank::create([
            'title'=>'Política de Uso Responsable del Teléfono Celular en el Trabajo',
            'is_question'=> 0,
            'parent_id'=> 56,
            'content_id'=> 1,
        ]);
        Bank::create([
            'title'=>'Política de Ambiente de Trabajo Libre de Alcohol y Drogas.',
            'is_question'=> 0,
            'is_correct'=>1,
            'parent_id'=> 56,
            'content_id'=> 1,
        ]);
        //PREGUNTA13-61
        Bank::create([
            'title'=>'Solo podrán extender su jornada laboral diaria con la autorización de la máxima autoridad de su empresa en el lugar de trabajo. Es parte de la __________________.',
            'is_question'=> 1,
            'content_id'=> 1,
        ]);
        Bank::create([
            'title'=>'Política del Comportamiento Seguro y Cultura de Seguridad.',
            'is_question'=> 0,
            'parent_id'=> 61,
            'content_id'=> 1,
        ]);
        Bank::create([
            'title'=>'Política de Uso Responsable del Teléfono Celular en el Trabajo',
            'is_question'=> 0,
            'parent_id'=> 61,
            'content_id'=> 1,
        ]);
        Bank::create([
            'title'=>'Política de Ambiente de Trabajo Libre de Alcohol y Drogas.',
            'is_question'=> 0,
            'parent_id'=> 61,
            'content_id'=> 1,
        ]);
        Bank::create([
            'title'=>'Política de Accidentes por causa fatiga y somnolencia.',
            'is_question'=> 0,
            'parent_id'=> 61,
            'is_correct'=> 1,
            'content_id'=> 1,
        ]);
        //PREGUNTA14-66
        Bank::create([
            'title'=>'Así como, al subir o bajar escaleras y al transitar o caminar en zonas industriales y operacionales, con riesgo de atropello o contacto con energías peligrosas. Es parte de la __________________.',
            'is_question'=> 1,
            'content_id'=> 1,
        ]);
        Bank::create([
            'title'=>'Política del Derecho a Decir NO.',
            'is_question'=> 0,
            'parent_id'=> 66,
            'content_id'=> 1,
        ]);
        Bank::create([
            'title'=>'Política del Comportamiento Seguro y Cultura de Seguridad.',
            'is_question'=> 0,
            'parent_id'=> 66,
            'content_id'=> 1,
        ]);
        Bank::create([
            'title'=>'Política de Uso Responsable del Teléfono Celular en el Trabajo',
            'is_question'=> 0,
            'is_correct'=>1,
            'parent_id'=> 66,
            'content_id'=> 1,
        ]);
        Bank::create([
            'title'=>'Política de Ambiente de Trabajo Libre de Alcohol y Drogas.',
            'is_question'=> 0,
            'parent_id'=> 66,
            'content_id'=> 1,
        ]);
         //PREGUNTA15-71
        Bank::create([
            'title'=>'Difundir y respetar el “Derecho a Decir NO” cuando se ponga en riesgo la vida o la salud de un colaborador u otras personas. Es parte de la __________________.',
            'is_question'=> 1,
            'content_id'=> 1,
        ]);
        Bank::create([
            'title'=>'Política del Derecho a Decir NO.',
            'is_question'=> 0,
            'parent_id'=> 71,
            'content_id'=> 1,
        ]);
        Bank::create([
            'title'=>'Política del Comportamiento Seguro y Cultura de Seguridad.',
            'is_question'=> 0,
            'parent_id'=> 71,
            'content_id'=> 1,
        ]);
        Bank::create([
            'title'=>'Política de Uso Responsable del Teléfono Celular en el Trabajo',
            'is_question'=> 0,
            'parent_id'=> 71,
            'content_id'=> 1,
        ]);
        Bank::create([
            'title'=>'Política de Ambiente de Trabajo Libre de Alcohol y Drogas.',
            'is_question'=> 0,
            'parent_id'=> 71,
            'content_id'=> 1,
        ]);
        Bank::create([
            'title'=>'ERROR-AYB',
            'is_question'=> 0,
            'is_correct'=>1,
            'parent_id'=> 71,
            'content_id'=> 1,
        ]);
        //PREGUNTA16-76
        Bank::create([
            'title'=>'Forman parte de los EPP Covid 19:',
            'is_question'=> 1,
            'content_id'=> 1,
        ]);
        Bank::create([
            'title'=>'Casco, chaleco, botas',
            'is_question'=> 0,
            'parent_id'=> 76,
            'content_id'=> 1,
        ]);
        Bank::create([
            'title'=>'Casco, mascarillas, protector facial.',
            'is_question'=> 0,
            'is_correct' => 1,
            'parent_id'=> 76,
            'content_id'=> 1,
        ]);
        Bank::create([
            'title'=>'Casco, casaca, chaleco.',
            'is_question'=> 0,
            'parent_id'=> 76,
            'content_id'=> 1,
        ]);
        Bank::create([
            'title'=>'Casco, lámpara minera, casaca.',
            'is_question'=> 0,
            'parent_id'=> 76,
            'content_id'=> 1,
        ]);

        //PREGUNTA17-81
        Bank::create([
            'title'=>'La identificación de peligros, evaluación de riesgos y controles es tarea de___________.',
            'is_question'=> 1,
            'content_id'=> 1,
        ]);
        Bank::create([
            'title'=>'El Titular de la actividad minera.',
            'is_question'=> 0,
            'parent_id'=> 81,
            'content_id'=> 1,
        ]);
        Bank::create([
            'title'=>'El titular de la actividad minera y todos los trabajadores.',
            'is_question'=> 0,
            'is_correct'=>1,
            'parent_id'=> 81,
            'content_id'=> 1,
        ]);
        Bank::create([
            'title'=>'El supervisor y la empresa.',
            'is_question'=> 0,
            'parent_id'=> 81,
            'content_id'=> 1,
        ]);
        Bank::create([
            'title'=>'La empresa y el contratista.',
            'is_question'=> 0,
            'parent_id'=> 81,
            'content_id'=> 1,
        ]);
        //PREGUNTA18-86
        Bank::create([
            'title'=>'Al inicio de toda tarea los trabajadores deben de identificar los peligros, evaluar los riesgos y controles. ',
            'is_question'=> 1,
            'content_id'=> 1,
        ]);
        Bank::create([
            'title'=>'Verdadero',
            'is_question'=> 0,
            'is_correct'=>1,
            'parent_id'=> 86,
            'content_id'=> 1,
        ]);
        Bank::create([
            'title'=>'Falso.',
            'is_question'=> 0,
            'parent_id'=> 86,
            'content_id'=> 1,
        ]);
        //PREGUNTA19-89
        Bank::create([
            'title'=>'Los exámenes médicos pre-ocupacionales, anuales y de retiro se toma a:',
            'is_question'=> 1,
            'content_id'=> 1,
        ]);
        Bank::create([
            'title'=>'Los supervisores',
            'is_question'=> 0,
            'parent_id'=> 89,
            'content_id'=> 1,
        ]);
        Bank::create([
            'title'=>'Todos los trabajadores',
            'is_question'=> 0,
            'is_correct'=>1,
            'parent_id'=> 89,
            'content_id'=> 1,
        ]);
        Bank::create([
            'title'=>'Los contratistas',
            'is_question'=> 0,
            'parent_id'=> 89,
            'content_id'=> 1,
        ]);
        Bank::create([
            'title'=>'La gerencia.',
            'is_question'=> 0,
            'parent_id'=> 89,
            'content_id'=> 1,
        ]);
        //PREGUNTA20-94
        Bank::create([
            'title'=>'Cuál de estos trabajos que requieren obligatoriamente del Permiso escrito de trabajo de alto riesgo. PETAR.',
            'is_question'=> 1,
            'content_id'=> 1,
        ]);
        Bank::create([
            'title'=>'Trabajos en espacios confinados. ',
            'is_question'=> 0,
            'parent_id'=> 94,
            'content_id'=> 1,
        ]);
        Bank::create([
            'title'=>'Trabajos en caliente. ',
            'is_question'=> 0,
            'parent_id'=> 94,
            'content_id'=> 1,
        ]);
        Bank::create([
            'title'=>'Excavaciones mayores o iguales de 0.5 metros. ',
            'is_question'=> 0,
            'parent_id'=> 94,
            'content_id'=> 1,
        ]);
        Bank::create([
            'title'=>'ERROR	A y B',
            'is_question'=> 0,
            'is_correct'=>1,
            'parent_id'=> 94,
            'content_id'=> 1,
        ]);
        //PREGUNTA21-99
        Bank::create([
            'title'=>'Los incidentes peligrosos y/o situaciones de emergencia y accidentes mortales, no deben ser notificados por el titular de actividad minera, dentro de las veinticuatro (24) horas de ocurridos, en el formato del ANEXO 21',
            'is_question'=> 1,
            'content_id'=> 1,
        ]);
        Bank::create([
            'title'=>'Verdadero',
            'is_question'=> 0,
            'parent_id'=> 99,
            'content_id'=> 1,
        ]);
        Bank::create([
            'title'=>'Falso',
            'is_question'=> 0,
            'is_correct'=>1,
            'parent_id'=> 99,
            'content_id'=> 1,
        ]);

        //PREGUNTA22-102
        Bank::create([
            'title'=>'De acuerdo a Ley, la empresa tiene el compromiso de brindar apoyo  a sus trabajadores: vivienda, mejoramiento de escuelas y educación, recreación, asistencia social, asistencia médica hospitalaria.',
            'is_question'=> 1,
            'content_id'=> 1,
        ]);
        Bank::create([
            'title'=>'Verdadero',
            'is_question'=> 0,
            'parent_id'=> 102,
            'content_id'=> 1,
        ]);
        Bank::create([
            'title'=>'Falso',
            'is_question'=> 0,
            'is_correct'=>1,
            'parent_id'=> 102,
            'content_id'=> 1,
        ]);
        Bank::create([
            'title'=>'ERROR ESTE SI BORRAR',
            'is_question'=> 0,
            'parent_id'=> 102,
            'content_id'=> 1,
        ]);

        ///Se Termino el Primer Examen

        //Segundo Examen
        //corregido

        //PREGUNTA1-106
        Bank::create([
            'title'=>'¿El comité de seguridad y salud ocupacional es un órgano paritario?  ',
            'is_question'=> 1,
            'content_id'=> 2,
        ]);
        Bank::create([
            'title'=>'Porque tiene igual número de representantes del empleador y de los trabajadores.',
            'is_question'=> 0,
            'is_correct'=> 1,
            'parent_id'=> 106,
            'content_id'=> 2,
        ]);
        Bank::create([
            'title'=>'Porque tiene representantes del empleador y de los trabajadores.',
            'is_question'=> 0,
            'parent_id'=> 106,
            'content_id'=> 2,
        ]);
        Bank::create([
            'title'=>'Porque tiene Miembros Titulares y Suplentes.',
            'is_question'=> 0,
            'parent_id'=> 106,
            'content_id'=> 2,
        ]);
        Bank::create([
            'title'=>'Porque tiene representantes de los trabajadores.',
            'is_question'=> 0,
            'parent_id'=> 106,
            'content_id'=> 2,
        ]);
        Bank::create([
            'title'=>'Ninguna de las anteriores.',
            'is_question'=> 0,
            'parent_id'=> 106,
            'content_id'=> 2,
        ]);
         //PREGUNTA2-112
        Bank::create([
            'title'=>'El número de personas que componen el Comité de Seguridad y Salud Ocupacional es:',
            'is_question'=> 1,
            'content_id'=> 2,
        ]);
        Bank::create([
            'title'=>'4 mínimo  y 10 máximo',
            'is_question'=> 0,
            'parent_id'=> 112,
            'content_id'=> 2,
        ]);
        Bank::create([
            'title'=>'4 mínimo y 6 máximo.',
            'is_question'=> 0,
            'parent_id'=> 112,
            'content_id'=> 2,
        ]);
        Bank::create([
            'title'=>'4 mínimo y 12 máximo.',
            'is_question'=> 0,
            'is_correct'=>1,
            'parent_id'=> 112,
            'content_id'=> 2,
        ]);
        Bank::create([
            'title'=>'4 mínimo y 15 máximo.',
            'is_question'=> 0,
            'parent_id'=> 112,
            'content_id'=> 2,
        ]);
        //PREGUNTA3-117
        Bank::create([
            'title'=>'Los requisitos para ser integrante  del comité de SSO es: ',
            'is_question'=> 1,
            'content_id'=> 2,
        ]);
        Bank::create([
            'title'=>'Ser trabajador del empleador y tener 21 años de edad como máximo.',
            'is_question'=> 0,
            'parent_id'=> 118,
            'content_id'=> 2,
        ]);
        Bank::create([
            'title'=>'Tener 20 años como mínimo.',
            'is_question'=> 0,
            'parent_id'=> 118,
            'content_id'=> 2,
        ]);
        Bank::create([
            'title'=>'Ser trabajador del empleador y tener 18 años cumplidos.',
            'is_question'=> 0,
            'is_correct'=>1,
            'parent_id'=> 118,
            'content_id'=> 2,
        ]);
        Bank::create([
            'title'=>'Ser trabajador del empleador y tener 18 años de edad como mínimo.',
            'is_question'=> 0,
            'parent_id'=> 118,
            'content_id'=> 2,
        ]);


        //PREGUNTA4-122
        Bank::create([
            'title'=>'Las funciones del comité de SSO son: ',
            'is_question'=> 1,
            'content_id'=> 2,
        ]);
        Bank::create([
            'title'=>'Aprobar el Programa Anual de Seguridad y Salud en el Trabajo.',
            'is_question'=> 0,
            'parent_id'=> 124,
            'content_id'=> 2,
        ]);
        Bank::create([
            'title'=>'Aprobar el Reglamento Interno de Seguridad y Salud del empleador.',
            'is_question'=> 0,
            'parent_id'=> 124,
            'content_id'=> 2,
        ]);
        Bank::create([
            'title'=>'Aprobar el plan anual de capacitación de los trabajadores sobre seguridad y salud en el trabajo.',
            'is_question'=> 0,
            'parent_id'=> 124,
            'content_id'=> 2,
        ]);
        Bank::create([
            'title'=>'Reunirse mensualmente en forma ordinaria para analizar y evaluar el avance de los objetivos establecidos en el programa anual.',
            'is_question'=> 0,
            'parent_id'=> 124,
            'content_id'=> 2,
        ]);
        Bank::create([
            'title'=>'Todas las anteriores.',
            'is_correct'=>1,
            'is_question'=> 0,
            'parent_id'=> 124,
            'content_id'=> 2,
        ]);

        //PREGUNTA5-128
        Bank::create([
            'title'=>'Los integrantes que conforman el comité de SSO son: ',
            'is_question'=> 1,
            'content_id'=> 2,
        ]);
        Bank::create([
            'title'=>'El presidente, el secretario, los miembros.',
            'is_question'=> 0,
            'parent_id'=> 128,
            'content_id'=> 2,
        ]);
        Bank::create([
            'title'=>'El presidente y los miembros.',
            'is_question'=> 0,
            'parent_id'=> 128,
            'content_id'=> 2,
        ]);
        Bank::create([
            'title'=>'El presidente, el secretario, los miembros y el observador.',
            'is_question'=> 0,
            'is_correct'=>1,
            'parent_id'=> 128,
            'content_id'=> 2,
        ]);
        Bank::create([
            'title'=>'Los miembros titulares y suplentes.',
            'is_question'=> 0,
            'parent_id'=> 128,
            'content_id'=> 2,
        ]);

        //PREGUNTA6-133
        Bank::create([
            'title'=>'Según el D.S 005-2012-TR: ',
            'is_question'=> 1,
            'content_id'=> 2,
        ]);
        Bank::create([
            'title'=>'Empleadores con más de 15 trabajadores deberán de elaborar su RISST.',
            'is_question'=> 0,
            'parent_id'=> 133,
            'content_id'=> 2,
        ]);
        Bank::create([
            'title'=>'Empleadores con 20 o más trabajadores deberán elaborar su RISST',
            'is_question'=> 0,
            'is_correct'=>1,
            'parent_id'=> 133,
            'content_id'=> 2,
        ]);

        Bank::create([
            'title'=>'Empleadores que cuenten con 20 trabajadores deberán elaborar su RISST.',
            'is_question'=> 0,
            'parent_id'=> 133,
            'content_id'=> 2,
        ]);
        Bank::create([
            'title'=>'Empleadores con 25 o más trabajadores deberán elaborar su RISST.',
            'is_question'=> 0,
            'parent_id'=> 133,
            'content_id'=> 2,
        ]);

        //PREGUNTA7  -138
        Bank::create([
            'title'=>'¿Qué es un programa Anual de seguridad y salud ocupacional?',
            'is_question'=> 1,
            'content_id'=> 2,
        ]);
        Bank::create([
            'title'=>'Es un conjunto de actividades de prevención que se debe de realizar en la empresa.',
            'is_question'=> 0,
            'parent_id'=> 138,
            'content_id'=> 2,
        ]);
        Bank::create([
            'title'=>'Establece realizar actividades no relacionadas con la seguridad y salud.',
            'is_question'=> 0,
            'parent_id'=> 138,
            'content_id'=> 2,
        ]);
        Bank::create([
            'title'=>'Es un Conjunto de actividades de prevención en Seguridad y Salud en el Trabajo que establece la organización, servicio o empresa para ejecutar a lo largo de un año.',
            'is_question'=> 0,
            'is_correct'=>1,
            'parent_id'=> 138,
            'content_id'=> 2,
        ]);
        Bank::create([
            'title'=>'Es un conjunto de actividades de locación que se debe de realizar en la empresa',
            'is_question'=> 0,
            'parent_id'=> 138,
            'content_id'=> 2,
        ]);
        //PREGUNTA8-143
        Bank::create([
            'title'=>'Todo programa Anual de seguridad y salud ocupacional debe ser evaluado:',
            'is_question'=> 1,
            'content_id'=> 2,
        ]);
        Bank::create([
            'title'=>'Anualmente',
            'is_question'=> 0,
            'is_correct'=> 1,
            'parent_id'=> 143,
            'content_id'=> 2,
        ]);
        Bank::create([
            'title'=>'Mensualmente',
            'is_question'=> 0,
            'parent_id'=> 143,
            'content_id'=> 2,
        ]);
        Bank::create([
            'title'=>'Trimestralmente',
            'is_question'=> 0,
            'parent_id'=> 143,
            'content_id'=> 2,
        ]);
        Bank::create([
            'title'=>'Bimensualmente',
            'is_question'=> 0,
            'parent_id'=> 143,
            'content_id'=> 2,
        ]);
        //PREGUNTA13-148
        Bank::create([
            'title'=>'El comité de seguridad y salud ocupacional se reúne: ',
            'is_question'=> 1,
            'content_id'=> 2,
        ]);
        Bank::create([
            'title'=>'En forma ordinaria una vez por mes, en día previamente fijado.',
            'is_question'=> 0,
            'parent_id'=> 148,
            'content_id'=> 2,
        ]);
        Bank::create([
            'title'=>'En forma ordinaria dos veces en un mes, en días previamente fijados.',
            'is_question'=> 0,
            'parent_id'=> 148,
            'content_id'=> 2,
        ]);
        Bank::create([
            'title'=>'En forma ordinaria una vez por mes en día previamente fijado, y de forma extraordinaria el comité se reúne a convocatoria de su presidente.',
            'is_question'=> 0,
            'is_correct'=>1,
            'parent_id'=> 148,
            'content_id'=> 2,
        ]);
        Bank::create([
            'title'=>'En forma ordinaria tres veces en un mes, en días previamente fijados.',
            'is_question'=> 0,
            'parent_id'=> 148,
            'content_id'=> 2,
        ]);
        //PREGUNTA10-153
        Bank::create([
            'title'=>'Uno de los miembros del comité es el presidente; su función es:',
            'is_question'=> 1,
            'content_id'=> 2,
        ]);
        Bank::create([
            'title'=>'Es el encargado de convocar, presidir y dirigir las reuniones del Comité de Seguridad y Salud en el Trabajo.',
            'is_question'=> 0,
            'is_correct'=> 1,
            'parent_id'=> 153,
            'content_id'=> 2,
        ]);
        Bank::create([
            'title'=>'Representa al Sindicato ante el empleador.',
            'is_question'=> 0,
            'parent_id'=> 153,
            'content_id'=> 2,
        ]);
        Bank::create([
            'title'=>'Evaluar las reuniones con el empleador.',
            'is_question'=> 0,
            'parent_id'=> 153,
            'content_id'=> 2,
        ]);
        Bank::create([
            'title'=>'Ninguna de las anteriores.',
            'is_question'=> 0,
            'parent_id'=> 153,
            'content_id'=> 2,
        ]);
         //PREGUNTA15-158
        Bank::create([
            'title'=>'¿Qué es el comité de SSO? ',
            'is_question'=> 1,
            'content_id'=> 2,
        ]);
        Bank::create([
            'title'=>'Representa al comité frente al empleador',
            'is_question'=> 0,
            'parent_id'=> 158,
            'content_id'=> 2,
        ]);
        Bank::create([
            'title'=>'Es el encargado de convocar, presidir y dirigir las reuniones del Comité de Seguridad y Salud en el Trabajo.',
            'is_question'=> 0,
            'parent_id'=> 158,
            'content_id'=> 2,
        ]);
        Bank::create([
            'title'=>'Organo bipartito y paritario constituido por representantes del empleador y de los trabajadores.',
            'is_question'=> 0,
            'is_correct'=>1,
            'parent_id'=> 158,
            'content_id'=> 2,
        ]);
        Bank::create([
            'title'=>'Todas las anteriores.',
            'is_question'=> 0,
            'parent_id'=> 158,
            'content_id'=> 2,
        ]);

        //PREGUNTA16-162
        Bank::create([
            'title'=>'Estructura del Comité de SSO ',
            'is_question'=> 1,
            'content_id'=> 2,
        ]);
        Bank::create([
            'title'=>'Gerente general, Gerente de SSO, Medico Ocupacional, Otros integrantes designados y representantes de los trabajadores. ',
            'is_question'=> 0,
            'is_correct'=> 1,
            'parent_id'=> 162,
            'content_id'=> 2,
        ]);
        Bank::create([
            'title'=>'Gerente de mina, gerente de recursos humanos, policía nacional otros representantes designados.',
            'is_question'=> 0,
            'parent_id'=> 162,
            'content_id'=> 2,
        ]);
        Bank::create([
            'title'=>'Gerente proyectos, Gerente de SSO, Medico ocupacional, Gerente RRHH y representantes de los trabajadores',
            'is_question'=> 0,
            'parent_id'=> 162,
            'content_id'=> 2,
        ]);
        Bank::create([
            'title'=>'Todas las anteriores.',
            'is_question'=> 0,
            'parent_id'=> 162,
            'content_id'=> 2,
        ]);

        //PREGUNTA13-167
        Bank::create([
            'title'=>'Proceso de elección de los representantes.',
            'is_question'=> 1,
            'content_id'=> 2,
        ]);
        Bank::create([
            'title'=>'Nominación de los candidatos se efectuará 15 días antes de la convocatoria',
            'is_question'=> 0,
            'is_correct'=> 1,
            'parent_id'=> 167,
            'content_id'=> 2,
        ]);
        Bank::create([
            'title'=>'Nominación de los candidatos se efectuará 13 días antes de la convocatoria',
            'is_question'=> 0,
            'parent_id'=> 167,
            'content_id'=> 2,
        ]);
        Bank::create([
            'title'=>'Nominación de los candidatos se efectuará 16 días antes de la convocatoria',
            'is_question'=> 0,
            'parent_id'=> 167,
            'content_id'=> 2,
        ]);
        Bank::create([
            'title'=>'Nominación de los candidatos se efectuará 12 días antes de la convocatoria',
            'is_question'=> 0,
            'parent_id'=> 167,
            'content_id'=> 2,
        ]);
        //
        //Termino del segundo examen COMITÉ DE SEGURIDAD Y SALUD OCUPACIONAL
        //Inicio del INSPECCIONES DE SEGURIDAD

        //PREGUNTA14-172
        Bank::create([
            'title'=>'¿Qué es el Programa Anual de SSO? ',
            'is_question'=> 1,
            'content_id'=> 2,
        ]);
        Bank::create([
            'title'=>'Documento que contiene el conjunto de actividades a desarrollar a lo largo de un semestre al año.',
            'is_question'=> 0,
            'parent_id'=> 172,
            'content_id'=> 2,
        ]);
        Bank::create([
            'title'=>'Documento que contiene el conjunto de actividades a desarrollar a lo largo de un periodo al año',
            'is_question'=> 0,
            'parent_id'=> 172,
            'content_id'=> 2,
        ]);
        Bank::create([
            'title'=>'Documento que contiene el conjunto de actividades a desarrollar a lo largo de un (01) año.',
            'is_question'=> 0,
            'is_correct'=>1,
            'parent_id'=> 172,
            'content_id'=> 2,
        ]);
        Bank::create([
            'title'=>'Documento que contiene el conjunto de actividades a desarrollar a lo largo de un trimestre al año.',
            'is_question'=> 0,
            'parent_id'=> 172,
            'content_id'=> 2,
        ]);
        //PREGUNTA15-177
        Bank::create([
            'title'=>'¿Cuántas son las políticas en nuestras Unidades? ',
            'is_question'=> 1,
            'content_id'=> 2,
        ]);
        Bank::create([
            'title'=>'5',
            'is_question'=> 0,
            'parent_id'=> 177,
            'content_id'=> 2,
        ]);
        Bank::create([
            'title'=>'8',
            'is_question'=> 0,
            'parent_id'=> 177,
            'content_id'=> 2,
        ]);
        Bank::create([
            'title'=>'7',
            'is_question'=> 0,
            'is_correct'=>1,
            'parent_id'=> 177,
            'content_id'=> 2,
        ]);
        Bank::create([
            'title'=>'6',
            'is_question'=> 0,
            'parent_id'=> 177,
            'content_id'=> 2,
        ]);
        //PREGUNTA16-182
        Bank::create([
            'title'=>'Obligaciones de los Supervisores',
            'is_question'=> 1,
            'content_id'=> 2,
        ]);

        Bank::create([
            'title'=>'Evaluar y no implementar medidas de control requeridas en caso que el trabajador ejerza su derecho a decir NO',
            'is_question'=> 0,
            'parent_id'=> 182,
            'content_id'=> 2,
        ]);
        Bank::create([
            'title'=>'Investigar situaciones que se consideren peligrosas.',
            'is_question'=> 0,
            'parent_id'=> 182,
            'content_id'=> 2,
        ]);
        Bank::create([
            'title'=>'Informar a los trabajadores acerca de los peligros.',
            'is_question'=> 0,
            'is_correct'=>1,
            'parent_id'=> 182,
            'content_id'=> 2,
        ]);
        Bank::create([
            'title'=>'Investigar situaciones que se consideren peligrosas ',
            'is_question'=> 0,
            'parent_id'=> 182,
            'content_id'=> 2,
        ]);
        //PREGUNTA17-187
        Bank::create([
            'title'=>'Obligaciones de los Trabajadores',
            'is_question'=> 1,
            'content_id'=> 2,
        ]);
        Bank::create([
            'title'=>'Utilizar no correctamente máquinas, equipos, herramientas.',
            'is_question'=> 0,
            'parent_id'=> 187,
            'content_id'=> 2,
        ]);
        Bank::create([
            'title'=>'Ser responsables por su seguridad y las de sus compañeros.',
            'is_question'=> 0,
            'is_correct'=>1,
            'parent_id'=> 187,
            'content_id'=> 2,
        ]);
        Bank::create([
            'title'=>'No informar a los trabajadores acerca de los peligros.',
            'is_question'=> 0,
            'parent_id'=> 187,
            'content_id'=> 2,
        ]);
        Bank::create([
            'title'=>'Utilizar el IPERC de un día anterior',
            'is_question'=> 0,
            'parent_id'=> 187,
            'content_id'=> 2,
        ]);
        //PREGUNTA18-192
        Bank::create([
            'title'=>'Derecho de los Trabajadores',
            'is_question'=> 1,
            'content_id'=> 2,
        ]);
        Bank::create([
            'title'=>'Conocer los peligros y riesgos no existentes en el lugar de trabajo a través del IPERC.',
            'is_question'=> 0,
            'parent_id'=> 192,
            'content_id'=> 2,
        ]);
        Bank::create([
            'title'=>'Retirarse de cualquier área de trabajo al detectar un peligro de alto riesgo.',
            'is_question'=> 0,
            'is_correct'=>1,
            'parent_id'=> 192,
            'content_id'=> 2,
        ]);
        Bank::create([
            'title'=>'Investigar situaciones que se consideren peligrosas.',
            'is_question'=> 0,
            'parent_id'=> 192,
            'content_id'=> 2,
        ]);
        Bank::create([
            'title'=>'Retirarse del lugar de trabajo sin autorización.',
            'is_question'=> 0,
            'parent_id'=> 192,
            'content_id'=> 2,
        ]);
         //PREGUNTA19-197
        Bank::create([
            'title'=>'Derecho de los Trabajadores',
            'is_question'=> 1,
            'content_id'=> 2,
        ]);
        Bank::create([
            'title'=>'Conocer los peligros y riesgos no existentes en el lugar de trabajo a través del IPERC.',
            'is_question'=> 0,
            'parent_id'=> 197,
            'content_id'=> 2,
        ]);
        Bank::create([
            'title'=>'Retirarse de cualquier área de trabajo al detectar un peligro de alto riesgo.',
            'is_question'=> 0,
            'is_correct'=>1,
            'parent_id'=> 197,
            'content_id'=> 2,
        ]);
        Bank::create([
            'title'=>'Investigar situaciones que se consideren peligrosas.',
            'is_question'=> 0,
            'parent_id'=> 197,
            'content_id'=> 2,
        ]);
        Bank::create([
            'title'=>'todas las anteriores.',
            'is_question'=> 0,
            'parent_id'=> 197,
            'content_id'=> 2,
        ]);


    }
}
