@extends('layouts.app')

@section('banner')
<div class="banner-inicio">
    <img src="{{ asset('images/portada-inicio.png') }}" alt="">
</div>
@endsection

@section('content')

<div class="envoltorio">
    <div class="container">
        @if (session()->has('info'))
            <div class="alert alert-info">
                {{ session('info') }}
            </div>
        @endif
        <div class="login-title">
            <h1 class="">WELCOME</h1>
            <span>COME TO THE  VIRTUAL CAMPUS</span>
        </div>
        <div class="row">
            <div class="col-md-6 offset-md-3 col-lg-4 offset-lg-4">
                <div class="card" style="border-radius: 1.5rem">
                    <div style="text-align: center; margin-top: 1rem">
                        <img src="{{asset('images/logo.png')}}" height="60px" style="block-size: 3.75rem;" alt="">
                    </div>
                    <div class="card-body card__login">
                        <h5 class=""><strong>Enter the virtual campus</strong></h5>
                        <br>
                        <form method="POST" action="{{ route('login') }}">
                            @csrf
        
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fas fa-user"></i></span>
                                </div>
                                <input id="dni" type="text"
                                    class="form-control text-center @error('dni') is-invalid @enderror" name="dni" value="{{ old('dni') }}"
                                    required autocomplete="dni" autofocus
                                    placeholder="Please enter your ID number">
        
                                    @error('dni')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                            </div>
        
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fas fa-key"></i></span>
                                </div>
                                <input id="password" type="password" class="form-control text-center @error('password') is-invalid @enderror"
                                    name="password" required autocomplete="current-password"
                                    placeholder="Please enter password">
        
                                    @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                            </div>
        
                            <div class="form-group row text-center">
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn__primary btn-lg btn-block">
                                        <i class="fas fa-sign-in-alt"></i> {{ __('Login') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>    
</div>

@endsection
