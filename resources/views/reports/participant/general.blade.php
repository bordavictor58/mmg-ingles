@extends('layouts.participants.base')


@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="main-card mb-3 card">
            @include('layouts.message')
            <div class="card-header"><h3 class="text-center card-title text-primary">REPORTES</h3></div>
            <div class="card-body">
            <hr>
            <form action="{{route('report.general')}}" method="POST">
                @csrf
                <div class="form-row align-items-center">
                    <div class="form-group col-md-3 text-center">
                        <label>Fecha de Inicio</label>
                        <input type="date" autocomplete="off" class="form-control" name="start_date" value="">
                    </div>

                    <div class="form-group col-md-3 text-center">
                        <label>Fecha de Termino</label>
                        <input type="date" autocomplete="off" class="form-control" name="end_date" value="">
                    </div>

                    <div class="form-group col-md-6 text-center">
                        <label>Curso</label>
                        <select  class="custom-select mr-sm-2" name="course_id">
                            <option selected value="-1">Todos</option>
                            @foreach ($courses as $course)
                                <option value="{{ $course->id }}">{{ $course->name }}</option>
                            @endforeach
                        </select>
                    </div>



                </div>
                <div class="col-md-12 text-center">
                    <button type="submit"  class="btn btn__primary">Reporte Academico General</button>

                </div>
                </form>
            </div>

            <div class="card-footer text-center">
                <a   href="{{route('home')}}" class="btn btn__return"><img src="{{ asset('images/regresar.png') }}" width="50px" style="position: relative; right:20px;"> Regresar</a>
            </div>
        </div>
    </div>
</div>
@endsection
