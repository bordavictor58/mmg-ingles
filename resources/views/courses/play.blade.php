
@extends('layouts.template')


@section('content')
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div class="page-title-icon">
                    <i class="pe-7s-video icon-gradient bg-mean-fruit"></i>
                </div>
                <div>
                    {{ $classroom->course->name }}
                    <div class="page-title-subheading">
                        {{ $type->name }}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            @include('layouts.message')
            <div class="main-card mb-3 card">
                <div class="card-header card-header-tab">
                    <div class="card-header-title text-capitalize">
                        <i class="header-icon fa fa-th-list fa-xs icon-gradient bg-primary"></i>
                        {{ $content->name }}

                    </div>
                    <div class="btn-actions-pane-right text-capitalize">
                        @if(Auth::user()->hasRole('participante'))
                            <a href="{{ route('courses.participant.type', [$classroom->id, $type->id]) }}" class="btn-wide btn-outline-2x mr-md-2 btn btn-outline-focus btn-sm">
                                <i class="fa fa-undo-alt"></i> REGRESAR
                            </a>
                        @else
                            <a href="{{ route('courses.types.index', $course->id) }}" class="btn-wide btn-outline-2x mr-md-2 btn btn-outline-focus btn-sm">
                                <i class="fa fa-undo-alt"></i> REGRESAR
                            </a>
                        @endif
                    </div>
                </div>
                <div class="card-body">
                    <div class="container-fluid">
                                <div style="padding:42.19% 0 0 0;position:relative;">

                                    <iframe id="vimeo_video" src="https://player.vimeo.com/video/{{ $content->content_link }}?#t={{$inscription->current_time_video}}"
                                        style="position:absolute;top:0;left:0;width:102%;height:102%;"
                                        frameborder="0" allow="autoplay; fullscreen" allowfullscreen>
                                    </iframe>
                                </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('js')
    <!-- inicio -->
    <script>
        var iframe = document.querySelector('iframe');
        var player = new Vimeo.Player(iframe);
        $(document).ready(function() {

            function currentTimeVideo() {
                player.getCurrentTime().then(function(seconds) {
                    console.log(seconds);
                    if(Math.round(seconds)==0)
                    {
                        return false;
                    }
                    //loop
                    player.setLoop(true).then(function(loop) {

                        player.getDuration().then(function(duration) {

                            let min = secondsToString(seconds);
                            console.log(seconds);
                            if(Math.round(seconds)== Math.round(duration))
                            {
                                console.log('finish');
                                return false;
                            }
                            $.ajax({
                                url:'{{ route("video.time",[$classroom]) }}',
                                data:{'seconds':min},
                                method:'GET',
                            })

                        });
                    });

                });
        }

        setInterval(currentTimeVideo, 5000);

        });
        function secondsToString(seconds) {
            var hour = Math.floor(seconds / 3600);
            hour = (hour < 10)? '0' + hour : hour;
            var minute = Math.floor((seconds / 60) % 60);
            minute = (minute < 10)? '' + minute : minute;
            var second = seconds % 60;
            second = (second < 10)? '' + second.toFixed(0) : second.toFixed(0);
            // 6m5s
            return  minute + 'm' + second + 's';
        }

    </script>
    <!-- FIN -->
@endsection


