@extends('layouts.template')

@section('content')
    @include('layouts.title', array(
                                    'image'         =>  'images/icon-seeder/programacion.png',
                                    'icon'          => '',
                                    'title'         => 'INFORMACIÓN DEL CURSO',
                                    'description'   => 'para conectarse al curso necesita conectarse desde preferencia o tener una buena señal de datos',
                                    'href'          => ''))

    <div class="row">
        <div class="col-12">
            <div class="mb-3 card">
                <div class="card-header">
                    {{ $classroom->name }}
                </div>
                <div class="card-body">

                    <div class="row">
                    @if($key==0)
                        @foreach($types as $type)
                            <div class="col-sm-4 d-flex flex-column align-items-center mb-4 card-detail">
                                <div class="bg__{{ Str::camel($type->name) }} mb-3 p-2 rounded-circle card-detail__img">
                                    <a href="{{ route('courses.participant.type', [$classroom->id, $type->id]) }}">
                                        <img src="{{ asset('img/icons/'.Str::camel($type->name).'.png') }}" alt="{{ Str::camel($type->name) }}" class="">
                                    </a>
                                </div>
                                @if($type->id == 1)
                                    <span class="font-weight-bold card-detail__title">Descripción del Curso</span>
                                @else
                                    <span class="font-weight-bold card-detail__title">{{ $type->name }}</span>
                                @endif
                            </div>

                        @endforeach
                    @endif
                    @if($key==0)
                        @if(count($classroom->meetings) > 0)
                            <div class="col-sm-4 d-flex flex-column align-items-center mb-4 card-detail">
                                <div class="bg__reunion mb-3 p-2 rounded-circle card-detail__img">
                                </div>
                                <span class="font-weight-bold card-detail__title">Video Conferencia</span>
                            </div>
                        @endif
                    @endif

                            @if(count($classroom->tests) > 0)
                            <div class="col-sm-4 d-flex flex-column align-items-center mb-4 card-detail">
                                <div class="bg__test mb-3 p-2 rounded-circle card-detail__img">
                                    <a href="{{ route('classrooms.list',array($classroom,$key)) }}">
                                        <img src="{{ asset('img/icons/examen.png') }}" alt="examen" class="">
                                    </a>
                                </div>
                                @if($key==1)
                                <span class="font-weight-bold card-detail__title">Examen De Inicio</span>
                                @else
                                <span class="font-weight-bold card-detail__title">Examenes</span>
                                @endif
                            </div>
                            @endif

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
<script>
        var id_classroom = "<?php echo ''.$classroom->id ?>";
        var user_id = "<?php echo ''.Auth::user()->id ?>";
        var iframe = document.querySelector('iframe');
        var player = new Vimeo.Player(iframe);
        $(document).ready(function() {
            console.log('hola');
            function currentTimeVideo() {
                player.getCurrentTime().then(function(seconds) {
                    if(Math.round(seconds)==0)
                    {
                        return false;
                    }
                    //loop
                    player.setLoop(true).then(function(loop) {

                        player.getDuration().then(function(duration) {

                            let min = secondsToString(seconds);
                            if(Math.round(seconds)== Math.round(duration))
                            {
                                console.log('finish');
                                return false;
                            }
                            var objeto = {
                                user_id : user_id,
                                id_classroom: id_classroom,
                                seconds : min
                            }
                            socket.emit('escuchar-comunicacion',objeto);

                        });
                    });

                });
        }

        setInterval(currentTimeVideo, 5000);

        });
        function secondsToString(seconds) {
            var hour = Math.floor(seconds / 3600);
            hour = (hour < 10)? '0' + hour : hour;
            var minute = Math.floor((seconds / 60) % 60);
            minute = (minute < 10)? '' + minute : minute;
            var second = seconds % 60;
            second = (second < 10)? '' + second.toFixed(0) : second.toFixed(0);
            // 6m5s
            return  minute + 'm' + second + 's';
        }

    </script>

@endsection
