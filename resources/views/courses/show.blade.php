@extends('layouts.participants.base')

@section('content')


<div class="row">
    <div class="col-md-12">
        <div class="main-card mb-3 card">
            <div class="card-header">
                <h3 class="card-title text-center">CURSOS</h3>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    @include('layouts.message')
                    <table class="align-middle table table-sm table-hover">
                        <thead class="thead-blue">
                        <tr class="text-center">
                            <th scope="col" >#</th>
                            <th scope="col" >Curso</th>
                            <th scope="col" >Horas Dictadas</th>
                            <th scope="col" >Nota Minima</th>
                            <th scope="col" >Fecha Inicio</th>
                            <th scope="col" >Fecha Fin</th>
                            <th scope="col" >Imagen</th>
                            <th scope="col" >Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($classrooms as $classroom)
                        <tr class="text-center">
                            <td>{{$loop->iteration}}</td>
                            <td>{{ $classroom->name }}</td>
                            <td>{{ $classroom->hour }}</td>
                            <td>{{ $classroom->grade_min }}</td>
                            <td>{{ $classroom->start_datetime }}</td>
                            <td>{{ $classroom->end_datetime }}</td>
                            <td>
                                <img  style= "width:100px;  background-color: #EFEFEF;" class="img-rounded"
                                src="{{ substr($classroom->course->image(),6)}}" alt="">
                            </td>

                            <td style="white-space:nowrap;">
                                <a href="{{ route('course.detail',[$classroom])}}"  class="btn btn__small__success" data-name="{{ $classroom->name }} - {{ $classroom->start_datetime }}">Detalle</a>
                                <a href="{{ route('home') }}" class="btn btn__small__primary"> Regresar</a>

                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<!-- <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h6 class="modal-title" id="exampleModalLabel">Modal title</h6>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer text-center">
        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div> -->

@endsection


@section('js')
<script>
    // $(".btn_modal").click(function(){
    //     console.log($(this).attr('data-name'));
    //     $("#exampleModalLabel").text($(this).attr('data-name'));
    // });
</script>
@endsection

