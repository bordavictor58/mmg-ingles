@extends('layouts.participants.base')

@section('content')
    <div class="row d-flex justify-content-center" style="border: 1px solid transparent">
        <h1 class="title__primary">Exam Detail: {{$test_user->test->name}}</h1>
    </div>
    <br>
@foreach($classroom->course->banks as $bank)
    <div class="card">
        <div class="card-body">
            <h6><strong>{{ $loop->iteration }}.- {{ $bank->title }}</strong></h6>
        </div>
    </div>

        <div class="card my-2">
            @foreach($bank->childs as $child)
            @if( in_array($child->id, $correct_answers))
                <div class="alert alert-success">
                <h5 class="alert-heading">正确</h5>

                <div class="form-check">
                    <label  for="{{ $child->id }}">
                        {{ $child->title }}
                    </label>
                </div>
                </div>
            @elseif(in_array($child->id, $incorrect_answers))
            <div class="alert alert-danger">
                <h5 class="alert-heading">错误</h5>

                <div class="form-check">
                    <label  for="{{ $child->id }}">
                        {{ $child->title }}
                    </label>
                </div>
                </div>
            @else
                <div class="form-check card my-1 mx-4">

                    <label class="" for="{{ $child->id }}">
                        {{ $child->title }}
                    </label>
                </div>
            @endif
            @endforeach
        </div>
    @endforeach
    <div class="row my-4">
        <div class="col">
            <div>

                <a href="{{route('participant.course.note',[$classroom,$test_user])}}" class="btn btn-primary btn-lg"><img src="{{ asset('images/regresar.png') }}" width="50px" style="position: relative; right:10px;" alt="play">return</a>
            </div>
        </div>
    </div>


@endsection
