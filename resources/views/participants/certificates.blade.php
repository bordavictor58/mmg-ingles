@extends('layouts.participants.base')

@section('content')

    <div class="row d-flex justify-content-center align-content-center flex-column" style="border: 1px solid transparent">
        <h1 class="title__primary">Mis Certificados</h1>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-sm table-hover">
                            <thead class="thead-light">
                            <tr  class="text-center">

                                <th>NRO</th>
                                <th>CURSO</th>
                                <th>NOTA FINAL</th>
                                <th>NOTA MINIMA</th>
                                <th>OPCIONES</th>
                            </tr>
                            </thead>
                            @foreach ($user->scheduled as $classroom)
                                <tr>
                                    <td class="text-center text-muted" >{{ $loop->iteration }}</td>
                                    <td>{{ $classroom->name }}</td>
                                    <td class="text-center">{{ $classroom->pivot->grade }}</td>
                                    <td class="text-center">{{ $classroom->pivot->grade_min }}</td>

                                    <td style="white-space:nowrap;">
                                        @if($classroom->pivot->grade_tried === null)
                                            <span class="text-black">Sin intentos</span>
                                        @else
                                            @if($classroom->pivot->grade >= $classroom->pivot->grade_min )
                                            <a class="btn btn-sm btn-success" style="line-height: 1.5; margin-right: 0; margin-bottom: 0; min-width: auto"
                                                href="{{ route('users.certificate.inscriptions', $classroom->pivot->id) }}">
                                                descargar
                                            </a>
                                            @else
                                                <span class="text-danger">Desaprobado</span>
                                            @endif

                                        @endif

                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
            <br>
            <div class="col-md-4">
                <a href="{{ route('home') }}" class="btn btn__info"><img src="{{ asset('images/regresar.png') }}" width="50px" style="position: relative; right:10px;" alt="play"> Regresar</a>

            </div>
        </div>
    </div>

@endsection

