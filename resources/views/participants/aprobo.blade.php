@extends('layouts.participants.base')

@section('content')
    <div class="container">
        <div class="row d-flex" style="border: 1px solid transparent; justify-content: center;">
            @if($classroom->course->subcategory_id!= null)
            <div>
                <img src="{{ asset('images/aprobado.png') }}" class="content-box__img d-none d-sm-block" alt="">
                <div class="content-box__button d-flex flex-column">

                    <h1 class="text-center">score： {{$test_user->grade}}</h1>
                    <h2 class="text-center"> You can try again, come on!</h2>
                    <img src="{{ asset('images/icon-aprobado.png') }}" class="img-fluid align-self-center my-5" style="height: 200px" alt="">
                    <div class="d-flex flex-column flex-sm-row">
                        {{-- <a href="{{ route('users.certificate.inscriptions',$inscription->id) }}" class="btn btn__info mr-1 mt-2"><img src="{{ asset('images/certificate.png') }}" width="50px" style="position: relative; right:10px;" alt="play">Certificado</a> --}}
                        <a href="{{route('participant.course.results',[$classroom,$test_user])}}" class="btn btn__secundary m-1"><img src="{{ asset('images/document.png') }}" width="50px" style="position: relative; right:10px;" alt="document">Review answers</a>
                        <a href="{{route('users.encuesta',[$classroom,$test_user])}}" class="btn btn__primary text-center m-1"><img src="{{ asset('images/encuesta.png') }}" width="50px" style="position: relative; right:10px;" alt="play">Please fill out the questionnaire</a>

                    </div>
                    <div class="d-block text-center pt-2">
                        @if($classroom2 != null)
                        <a href="{{ route('participant.course.saycheese', $classroom2)}}" class="btn btn__info m-1"><img src="{{ asset('images/avanzar.png') }}" width="50px" style="position: relative; right:20px;" alt="play">VIDEO TUTORIAL</a>
                        @else
                        <a href="{{route('home')}}" class="btn btn__info m-1"><img src="{{ asset('images/regresar.png') }}" width="50px" style="position: relative; right:20px;" alt="play">return</a>

                        @endif
                    </div>
                </div>
            </div>
            @else
            <div class="d-flex">
                <img src="{{ asset('images/aprobado.png') }}" class="content-box__img d-none d-sm-block" alt="">
                <div class="content-box__button d-flex flex-column">

                    <h1 class="text-center">： {{$test_user->grade}}</h1>
                    <h2 class="text-center">Congratulations, you passed!</h2>
                    <img src="{{ asset('images/icon-aprobado.png') }}" class="img-fluid align-self-center my-5" style="height: 200px" alt="">
                    <div class="d-flex flex-column flex-sm-row">
                        {{-- <a href="{{ route('users.certificate.inscriptions',$inscription->id) }}" class="btn btn__info mr-1 mt-2"><img src="{{ asset('images/certificate.png') }}" width="50px" style="position: relative; right:10px;" alt="play">Certificado</a> --}}
                            <a href="{{route('participant.course.results',[$classroom,$test_user])}}" class="btn btn__secundary m-1"><img src="{{ asset('images/document.png') }}" width="50px" style="position: relative; right:10px;" alt="document">Review answers</a>
                        @if($classroom->course->has_video_guia)
                            <a href="{{route('participants.video.guia',$classroom)}}" class="btn btn__info m-1"><img src="{{ asset('images/video_guia.png') }}" width="50px" style="position: relative; right:10px;" alt="play">VIDEO TUTORIAL</a>
                        @elseif(count($classroom->course->documents) >= 1)
                            <a href="{{route('participant.course.document',[Auth::user(),$classroom])}}" class="btn btn__info m-1"><img src="{{ asset('images/documento.png') }}" width="50px" style="position: relative; right:10px;" alt="play">Documents</a>

                        @else
                            <a href="{{route('home')}}" class="btn btn__info m-1"><img src="{{ asset('images/regresar.png') }}" width="50px" style="position: relative; right:10px;" alt="play">Return</a>
                        @endif
                    </div>
                    <div class="d-block text-center pt-2">
                        <a href="{{route('users.encuesta',[$classroom,$test_user])}}" class="btn btn__primary text-center"><img src="{{ asset('images/encuesta.png') }}" width="50px" style="position: relative; right:10px;" alt="play">Please fill out the questionnaire</a>
                    </div>
                </div>
            </div>
            @endif
        </div>
    </div>
@endsection

