@extends('layouts.participants.base')

@section('content')

    <div class="box-baner">
        <img class="box-baner__cover" src="{{asset('images/home/PORTADA PLATAFORMA CUMBRES1.svg')}}">
        <div class="content-box">

            <div class="content-box__info">
                <div class="button-group">
                    @role('participante')
                    <a href="{{ route('participant.course.listCategory') }}"
                        class="btn btn__home d-flex align-items-center justify-content-center">
                        <i class="fa fa-book mr-2" style="color: #1B6898; position: relative;right: 22px;"></i>
                        <span class="align-self-center">Mis Cursos</span>
                    </a>
                    <a href="{{ route('participant.course.certificates')}}" class="btn btn__home d-flex align-items-center justify-content-center">
                        <i class="fa fa-id-card mr-2"style="color: #1B6898; position: relative;right: 8px;"></i>
                        <span class="align-self-center">Certificados</span>
                    </a>
                    @endrole
                    @role('contratista')
                    <a href="{{ route('participant.report')}}" class="btn btn__info d-flex align-content-center justify-content-center">
                        <i class="fa fa-th-list fa-3x mr-3" style="color: #b3eecc; position: relative;right: 30px;"></i>
                        <span class="align-self-center">Reportes</span>
                    </a>
                    <a href="{{ route('participant.course.index')}}" class="btn btn__info d-flex align-content-center justify-content-center">
                        <i class="fa fa-book fa-3x mr-3" style="color: #b3eecc; position: relative;right: 6px;"></i>
                        <span class="align-self-center">Lista de Cursos</span>
                    </a>
                    @endrole
                </div>
            </div>
        </div>

    </div>


@endsection
