@extends('layouts.app')

@section('banner')
<div class="banner-inicio">
    <img src="{{ asset('images/portada-home.png') }}" alt="">
</div>
@endsection

@section('content')

<div class="envoltorio">
    <div class="container">
        @if (session()->has('info'))
            <div class="alert alert-info">
                {{ session('info') }}
            </div>
        @endif
        <div class="content-title" style="border: 1px solid transparent">
            <h1 class="title__primary mt-3">MY COURSES</h1>
        </div>
        <div class="courses">
        @foreach($categories as $category)
            <div class="course_style" style="background-color: #fff">
                <div class="course__content">
                    <div class="row">
                        <h3 class="course__title">{{$category->name}}</h3>
                    </div>

                    <div class="course__action">
                        <a href="{{ route('participant.course.list',$category) }}" class="btn btn__info mr-1 mt-2 mb-2">
                            <img src="{{ asset('images/into.png') }}" width="25px" style="position: relative; right:20px;" alt="play">
                            <span>LOGIN</span>
                        </a>
                        <a href="{{ route('home') }}" class="btn btn__secundary mr-2 mt-2 mb-2">
                            <img src="{{ asset('images/back.png') }}" width="25px" style="position: relative; right:10px;" alt="play">
                            <span>RETURN</span>
                        </a>
                    </div>
                </div>
            </div>
        @endforeach
            <div class="course_style" style="background-color: #fff">
                <div class="course__content">
                    <div class="row">
                        <h3 class="course__title">AVAILABLE FILES</h3>
                    </div>

                    <div class="course__action">
                        <a href="{{ route('participant.course.document',Auth::user()) }}" class="btn btn__info mr-1 mt-2 mb-2">
                            <img src="{{ asset('images/into.png') }}" width="25px" style="position: relative; right:20px;" alt="play">
                            <span>LOGIN</span>
                        </a>
                    </div>
                </div>
            </div>
            @role('contratista')
            <div class="course_style" style="background-color: #fff">
                <div class="course__content">
                    <div class="row">
                        <h3 class="course__title">Reportes</h3>
                    </div>

                    <div class="course__action">
                        <a href="{{ route('participant.report') }}" class="btn btn__info mr-1 mt-2 mb-2">
                            <img src="{{ asset('images/into.png') }}" width="25px" style="position: relative; right:20px;" alt="play">
                            <span>Ver Reportes</span>
                        </a>
                    </div>
                </div>
            </div>
            @endrole
        </div>
    </div>
</div>

@endsection
