@extends('layouts.participants.base')

@section('content')
    <div class="container">
        <div class="row" style="border: 1px solid transparent">
            <div class="d-flex">
                <img src="{{ asset('images/recuperacion.png') }}" class="content-box__img" alt="" class="">

                <div class="content-box__button d-flex flex-column">
                    <h1 class="text-center">score： {{$test_user->grade}}</h1>
                    <h2 class="text-center">You can try again, come on!</h2>
                    <img src="{{ asset('images/icon-recuperacion.png') }}" class="img-fluid align-self-center my-5" style="height: 200px" alt="">
                    <div class="d-flex">
                        <a href="{{route('participant.course.results',[$classroom,$test_user])}}" class="btn btn__info m-1"><img src="{{ asset('images/document.png') }}" width="50px" style="position: relative; right:10px;" alt="play">Review answers</a>
                        @if(count($inscription->grade_tried) >= $inscription->classroom->tests->first()->tried)
                        <a href="#" class="btn btn__secundary m-1 disabled"><img src="{{ asset('images/refresh.png') }}" width="50px" style="position: relative; right:10px;" alt="play">try again</a>

                        @else
                        <a href="{{route('participant.course.test',[$classroom,$test_user->test->id])}}" class="btn btn__secundary m-1"><img src="{{ asset('images/refresh.png') }}" width="50px" style="position: relative; right:10px;" alt="play">try again</a>

                        @endif

                    </div>
                    <div class="text-center">
                        <a href="{{route('home')}}" class="btn btn__info m-1"><img src="{{ asset('images/regresar.png') }}" width="50px" style="position: relative; right:20px;" alt="play">return</a>
                        <a href="{{route('users.encuesta',[$classroom,$test_user])}}" class="btn btn__primary text-center"><img src="{{ asset('images/encuesta.png') }}" width="50px" style="position: relative; right:10px;" alt="play">Please fill out the questionnaire</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

