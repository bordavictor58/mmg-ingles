@extends('layouts.template')

@section('content')

    @include('layouts.title', array(
        'icon'          => 'add-user',
        'title'         => 'Registrar Participante',
        'description'   => '',
        'button'        => 'Regresar',
        'href'          => 'participants.index',
        ))



    @include('layouts.message')
    <form action="{{ route('participants.store') }}" method="POST" id="formParticipant" enctype="multipart/form-data">
        @include('participants.partials._form', ['btnText' => 'Guardar','btnTitle'=>'Ingrese los datos del Participante'])
    </form>
@endsection

