@extends('layouts.participants.base')

@section('content')

        <div class="row">
            <div class="col-md-12">
                <div class="main-card mb-3 card">
                    <div class="card-header">
                        <div class="row d-flex justify-content-center" style="border: 1px solid transparent">
                            <h1 class="title__primary card-title">Questionnaire</h1>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="container-fluid">
                                    <div style="padding:42.19% 0 0 0;position:relative;">

                                                @if($classroom->course->id ==1)
                                                <!--Es identificacion de peligros -->
                                                    <iframe src="{{'https://docs.google.com/forms/d/e/1FAIpQLSdtvh-rx2h6C3DPhm_KDxLQJpRIg4t6Gr5au9J3PDPSrv-_Sw/viewform'}}?autoplay=1&byline=0&portrait=0"
                                                            style="position:absolute;top:0;left:0;width:100%;height:100%;"
                                                            frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                                                @endif
                                                @if($classroom->course->id ==2)
                                                <!--Es identificacion de peligros -->
                                                    <iframe src="{{'https://docs.google.com/forms/d/e/1FAIpQLScx10YIQYQfOC8_qNgv_EW7C1CApGtD8IUt_dzMCI9zVtHOsA/viewform'}}?autoplay=1&byline=0&portrait=0"
                                                            style="position:absolute;top:0;left:0;width:100%;height:100%;"
                                                            frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                                                @endif
                                                @if($classroom->id ==7)
                                                <!--IPERC -->
                                                    <iframe src="{{'https://docs.google.com/forms/d/e/1FAIpQLSc4nn_ZzSHe3zfsSeCpXAh8RdE0L0DZ59To3ejLbOmNTKtXXA/viewform'}}?autoplay=1&byline=0&portrait=0"
                                                            style="position:absolute;top:0;left:0;width:100%;height:100%;"
                                                            frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                                                @endif
                                                @if($classroom->id ==8)
                                                <!--HIGIENE  -->
                                                    <iframe src="{{'https://docs.google.com/forms/d/e/1FAIpQLSeBfnSKSwsNfZ2CBhMz20kByjmGhKr-LhipItkwHT_y1f-98Q/viewform'}}?autoplay=1&byline=0&portrait=0"
                                                            style="position:absolute;top:0;left:0;width:100%;height:100%;"
                                                            frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                                                @endif
                                                @if($classroom->id ==9)
                                                <!--RIESGOS ELECTRICOS1  -->
                                                    <iframe src="{{'https://docs.google.com/forms/d/e/1FAIpQLSd8HKljDGVGW7fnYeO4vmb11O579uF3Kx2zPC2IG7chDTIESw/viewform'}}?autoplay=1&byline=0&portrait=0"
                                                            style="position:absolute;top:0;left:0;width:100%;height:100%;"
                                                            frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                                                @endif
                                                @if($classroom->id ==10)
                                                <!--RI 2  -->
                                                    <iframe src="{{'https://docs.google.com/forms/d/e/1FAIpQLScbFV_RsvfaDxknA2Wm76AJbsqr3XIl7a6OCNL5ISFK9bkOmA/viewform'}}?autoplay=1&byline=0&portrait=0"
                                                            style="position:absolute;top:0;left:0;width:100%;height:100%;"
                                                            frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                                                @endif
                                                @if($classroom->id ==11)
                                                <!--PREVENCION Y PROTECCION CONTRA INCENDIOS  -->
                                                    <iframe src="{{'https://docs.google.com/forms/d/e/1FAIpQLScaNugm8ZlQ_r8dV5obuTQvB74Id8wldq-u0kWNgEaEntkAOQ/viewform'}}?autoplay=1&byline=0&portrait=0"
                                                            style="position:absolute;top:0;left:0;width:100%;height:100%;"
                                                            frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                                                @endif
                                                @if($classroom->id ==12)
                                                <!--AUDITORIA Y FISCALIZACIÓN  -->
                                                    <iframe src="{{'https://docs.google.com/forms/d/e/1FAIpQLScPOlnGtKfwQv1EUuiqxm7ELCXfNrN1UUYuzvB6B_WWwa8o9A/viewform'}}?autoplay=1&byline=0&portrait=0"
                                                            style="position:absolute;top:0;left:0;width:100%;height:100%;"
                                                            frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                                                @endif
                                                @if($classroom->id ==13)
                                                <!--EPP  -->
                                                    <iframe src="{{'https://docs.google.com/forms/d/e/1FAIpQLSdcABwp_wLthcGaHKB7nxzi64uYrWx0rVUKuZQVvsXl4nBmlQ/viewform'}}?autoplay=1&byline=0&portrait=0"
                                                            style="position:absolute;top:0;left:0;width:100%;height:100%;"
                                                            frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                                                @endif
                                    </div>
                                    <script src="https://player.vimeo.com/api/player.js"></script>

                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="d-block text-center">
                            <a href="{{route('participant.course.note',[$classroom,$test_user])}}" class="btn btn__primary text-center card-text"><img src="{{ asset('images/regresar.png') }}" width="50px" style="position: relative; right:10px;" alt="play">return</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

@endsection
