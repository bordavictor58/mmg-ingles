@extends('layouts.participants.base')

@section('content')

    <div class="content-title">
        <h1 class="title__primary">{{ $classroom->name }}</h1>
    </div>

    <div class="ed-video">
        <iframe id="vimeo_video" src="https://player.vimeo.com/video/{{ $classroom->course->has_video_guia }}"
            style="position:absolute;top:0;left:0;width:100%;height:100%;"
            frameborder="0" allow="autoplay; fullscreen" allowfullscreen>
        </iframe>
    </div>

    <div class="content-footer text-center">
            <a href="{{ route('participant.course.document',[Auth::user(),$classroom]) }}" class="btn btn__info_square"><img src="{{ asset('images/document.png') }}" width="45px" style="position: relative; right:8px;" alt="play">My Documents</a>
    </div>

@endsection

