@extends('layouts.participants.base')

@section('content')
    <div class="container">
        <div class="row" style="border: 1px solid transparent">
            @if($classroom->course->subcategory_id!= null)
            <div class="d-flex">
                <img src="{{ asset('images/desaprobado.png') }}" class="content-box__img" alt="" class="">
                <div class="content-box__button d-flex flex-column">
                    <h1 class="text-center">Tu Puntaje es de : {{$test_user->grade}}</h1>
                    <h2 class="text-center">lo sentimos :( Ud. ha desaprobado</h2>
                    <img src="{{ asset('images/icon-desaprobado.png') }}" class="img-fluid align-self-center my-5" style="height: 200px" alt="icon desaprobado">
                    <div class="d-flex">
                        <a href="{{route('participant.course.results',[$classroom,$test_user])}}" class="btn btn__info mr-1"><img src="{{ asset('images/document.png') }}" width="50px" style="position: relative; right:10px;" alt="play">Ver respuestas</a>
                        <a href="{{route('users.encuesta',[$classroom,$test_user])}}" class="btn btn__primary text-center"><img src="{{ asset('images/encuesta.png') }}" width="50px" style="position: relative; right:10px;" alt="play">Realizar Encuesta</a>
                    </div>
                    <div class="d-block text-center">
                        <a href="{{route('home')}}" class="btn btn__info"><img src="{{ asset('images/avanzar.png') }}" width="50px" style="position: relative; right:20px;" alt="play">return</a>

                    </div>
                </div>
            </div>
            @else
            <div class="d-flex">
                <img src="{{ asset('images/desaprobado.png') }}" class="content-box__img" alt="" class="">
                <div class="content-box__button d-flex flex-column">
                    <h1 class="text-center">Tu Puntaje es de : {{$test_user->grade}}</h1>
                    <h2 class="text-center">lo sentimos :( Ud. ha desaprobado</h2>
                    <img src="{{ asset('images/icon-desaprobado.png') }}" class="img-fluid align-self-center my-5" style="height: 200px" alt="icon desaprobado">
                    <div class="d-flex">
                        <a href="{{route('participant.course.results',[$classroom,$test_user])}}" class="btn btn__info mr-1"><img src="{{ asset('images/document.png') }}" width="50px" style="position: relative; right:10px;" alt="play">Ver respuestas</a>
                        <a href="{{ route('home') }}" class="btn btn__info mr-1"><img src="{{ asset('images/regresar.png') }}" width="40px" style="position: relative; right:5px;" alt="play">Regresar</a>
                    </div>
                    <div class="d-block text-center">

                        <a href="{{route('users.encuesta',[$classroom,$test_user])}}" class="btn btn__primary text-center"><img src="{{ asset('images/encuesta.png') }}" width="50px" style="position: relative; right:10px;" alt="play">Realizar Encuesta</a>
                    </div>
                </div>
            </div>
            @endif
        </div>
    </div>
@endsection

