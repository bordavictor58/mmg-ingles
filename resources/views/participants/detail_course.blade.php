@extends('layouts.participants.base')

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="main-card mb-3 card">
            <div class="card-header" style="height: 80px">
                <div class="d-flex">

                    <h5 class="card-title text-bold mr-5">{{ $classroom->name }} - {{ date('d/m/Y', strtotime($classroom->start_datetime)) }}</h5>

                    <a href="#" data-toggle="modal" data-target="#modal_export" class="btn btn__small__success mr-4"> Reporte Nota y Asistencia</a>
                    <a href="#" data-toggle="modal" data-target="#modal_export_general" class="btn btn__small__success"> Reporte General</a>

                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col col-md-4">
                        <div class="input-group">
                            <span class="input-group-text">Curso</span>
                            <input type="text" readonly class="form-control" value="{{ $classroom->name }}">
                        </div>
                    </div>
                    <div class="col col-md-3">
                        <div class="input-group">
                            <span class="input-group-text">Fecha de Inicio</span>
                            <input type="text" readonly class="form-control" value="{{ date('d/m/Y', strtotime($classroom->start_datetime)) }}">
                        </div>
                    </div>
                    <div class="col col-md-3">
                        <div class="input-group">
                            <span class="input-group-text">Fecha de Fin</span>
                            <input type="text" readonly class="form-control" value="{{ date('d/m/Y', strtotime($classroom->end_datetime)) }}">
                        </div>
                    </div>
                    <div class="col col-md-2">
                        <div class="input-group">
                            <span class="input-group-text">Horas</span>
                            <input type="text" readonly class="form-control" value="{{ $classroom->hour }}">
                        </div>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col col-md-4">
                        <div class="input-group">
                            <span class="input-group-text">Modalidad</span>
                            <input type="text" readonly class="form-control" value="{{ $classroom->modality }}">
                        </div>
                    </div>
                    <div class="col col-md-3">
                        <div class="input-group">
                            <span class="input-group-text">Nota Minima</span>
                            <input type="text" readonly class="form-control" value="{{ $classroom->grade_min }}">
                        </div>
                    </div>
                    <div class="col col-md-3">
                        <div class="input-group">
                            <span class="input-group-text">Tipo</span>
                            <input type="text" readonly class="form-control" value="{{ $classroom->type }}">
                        </div>
                    </div>
                </div>
                <div class="row mt-3 col-md-12">
                    <div class="table-responsive">
                        @include('layouts.message')
                        <table class="align-middle table table-sm table-striped">
                            <thead class="thead-blue">
                                <tr class="text-center">
                                    <th scope="col" >#</th>
                                    <th scope="col" >DNI/CE</th>
                                    <th scope="col" >Nombre</th>
                                    <th scope="col" >Empresa</th>
                                    <th scope="col" >Apellidos</th>
                                    <th scope="col" >Nota </th>
                                    <th scope="col" >Estado </th>
                                    <th scope="col" >Certificado </th>

                                </tr>
                            </thead>
                            <tbody>
                                @foreach($classroom->participants as $participant)
                                <tr class="text-center">
                                    <td>{{$loop->iteration}}</td>
                                    <td>{{$participant->dni}}</td>
                                    <td>{{$participant->full_name}}</td>
                                    <td>{{$participant->company->name}}</td>
                                    <td>{{$participant->last_name}}</td>
                                    <td>{{$participant->pivot->grade}}</td>
                                    @if($participant->pivot->grade >= $participant->pivot->grade_min)
                                    <td><span class="badge badge-success">APROBADO</span></td>
                                    <td><a  href="{{route('users.certificate.inscriptions',$participant->pivot->id)}}" class="badge badge-success">Descargar Certificado</a></td>
                                    @elseif($participant->pivot->grade== null)
                                    <td><span class="badge badge-info">PENDIENTE</span></td>
                                    <td></td>
                                    @else
                                    <td><span class="badge badge-danger">DESAPROBADO</span></td>
                                    <td></td>
                                    @endif
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="card-footer text-center" style="height: 70px">
                <a href="{{ route('participant.course.index') }}" class="btn btn__info">Regresar</a>

            </div>
        </div>
    </div>
</div>

<!--Modal-1-->
<div class="modal fade" id="modal_export" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
aria-hidden="true">
<div class="modal-dialog modal-notify " role="document">
<!--Content-->
<div class="modal-content">
    <!--Header-->
    <div class="modal-header">
        <p class="heading lead text-center">Descargar Reporte Nota y Asistencia</p>

    </div>
    <form id="form_modal" action="{{route('report.assistance')}}" method="post" >
        <!--Body-->
        {{ csrf_field() }}
        <div class="modal-body">
            <input type="hidden" name="course_id" value="{{$classroom->course->id}}">
                <div class="form-group">
                    <label for="startDate">Fecha Inicio</label>
                    <div class="input-group date">

                        <input type="date" name="start_date" class="form-control pull-right datepicker">
                    </div>
                </div>

                <div class="form-group">
                    <label for="startDate">Fecha Fin</label>
                    <div class="input-group date">

                        <input type="date" name="end_date" class="form-control pull-right datepicker">
                    </div>
                </div>
        </div>

        <!--Footer-->
        <div class="modal-footer justify-content-center" style="height: 180px">
            <button type="submit" id="btn_submit_export" class="btn btn__primary">Descargar</button>
            <a type="button" class="btn btn__info" data-dismiss="modal">Cancelar</a>
        </div>
    </form>
</div>
<!--/.Content-->
</div>
</div>

<!--Modal-2-->
<div class="modal fade" id="modal_export_general" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
aria-hidden="true">
<div class="modal-dialog modal-notify " role="document">
<!--Content-->
<div class="modal-content">
    <!--Header-->
    <div class="modal-header">
        <p class="heading lead text-center">Descargar Reporte Academico General</p>

    </div>
    <form id="form_modal" action="{{route('report.general')}}" method="post" >
        <!--Body-->
        {{ csrf_field() }}
        <div class="modal-body">
            <input type="hidden" name="course_id" value="{{$classroom->course->id}}">
                <div class="form-group">
                    <label for="startDate">Fecha Inicio</label>
                    <div class="input-group date">

                        <input type="date" name="start_date" class="form-control pull-right datepicker">
                    </div>
                </div>

                <div class="form-group">
                    <label for="startDate">Fecha Fin</label>
                    <div class="input-group date">

                        <input type="date" name="end_date" class="form-control pull-right datepicker">
                    </div>
                </div>
        </div>

        <!--Footer-->
        <div class="modal-footer justify-content-center" style="height: 180px">
            <button type="submit" id="btn_submit_export" class="btn btn__primary">Descargar</button>
            <a type="button" class="btn btn__info" data-dismiss="modal">Cancelar</a>
        </div>
    </form>
</div>
<!--/.Content-->
</div>
</div>

@endsection

@section('script')
<script>
    $( document ).ready(function() {


        $('.datepicker').datepicker({
            format: 'yyyy-mm-dd'
        });

    });
</script>
@endsection


