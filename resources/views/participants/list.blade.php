@extends('layouts.app')

@section('banner')
<div class="banner-inicio">
    <img src="{{ asset('images/portada-home.png') }}" alt="">
</div>
@endsection

@section('content')

<div class="envoltorio">
    <div class="container">
        @if (session()->has('info'))
            <div class="alert alert-info">
                {{ session('info') }}
            </div>
        @endif
        <div class="content-title" style="border: 1px solid transparent">
            <h1 class="title__primary">MY COURSES</h1>
        </div>
        <div class="courses">
        @foreach($user_log->inscriptions->unique('classroom.course.subcategory_id') as $inscription)

        @if($inscription->classroom->course->state==1)
            <div class="course_box" style="background-color: #fff">
                        <div class="course__content">
                            <h3 class="course__title2">@php
                                if($inscription->classroom->course->subcategory_id != null) echo $inscription->classroom->course->subcategory->name;
                                else echo $inscription->classroom->name;
                            @endphp</h3>
                            <div class="course__action">
                                <a href="{{ route('participant.course.saycheese', $inscription->classroom)}}" class="btn btn__secundary">
                                    <img src="{{ asset('images/play.png') }}" width="40px" alt="play">LOGIN</a>
                                <a href="{{URL::previous()}}" class="btn__terciario">
                                    <img src="{{ asset('images/back.png') }}" width="40px" alt="play">RETURN</a>
                            </div>
                        </div>
            </div>
        @endif  
        @endforeach
        @foreach($user->inscriptions->unique('classroom.course.name') as $inscription)

        @if($inscription->classroom->course->state==1)
        <div class="course_box" style="background-color: #fff">
                    <div class="course__content">
                        <h3 class="course__title2">@php
                            if($inscription->classroom->course->subcategory_id != null) echo $inscription->classroom->course->subcategory->name;
                            else echo $inscription->classroom->name;
                        @endphp</h3>
                        <div class="course__action">
                            <a href="{{ route('participant.course.saycheese', $inscription->classroom)}}" class="btn btn__secundary">
                                <img src="{{ asset('images/play.png') }}" width="40px" alt="play"> LOGIN</a>
                            <a href="{{URL::previous()}}" class="btn__terciario">
                                <img src="{{ asset('images/back.png') }}" width="40px" alt="play"> RETURN</a>
                        </div>
                    </div>
        </div>
        @endif
        @endforeach
    </div>
</div>

@endsection

