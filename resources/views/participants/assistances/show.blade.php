
@extends('layouts.participants.base')

@section('content')
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-actions">
                    <a href="{{ route('classrooms.index') }}" class="btn-shadow btn btn-info">
                        <span class="btn-icon-wrapper pr-1 opacity-8">
                            <i class="fa fa-undo-alt"></i>
                        </span>
                        REGRESAR
                    </a>

            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            @include('layouts.message')
            <div class="main-card mb-3 card">
                <div class="card-header card-header-tab">
                    <div class="card-header-title text-capitalize">
                        <i class="header-icon fa fa-th-list fa-xs "></i>
                        {{ $classroom->name }} / {{ $classroom->start_datetime }} / {{ $classroom->end_datetime }}
                        <ol>
                            <li> Relacion de {{$classroom->scheluded_participants_count }} participantes</li>
                        </ol>

                    </div>
                </div>
                <form action="{{ route('classrooms.assistances.register',$classroom->id) }}" method="POST">
                    @csrf
                    <div class="card-body">
                        <div class="table-responsive">
                        <table class="table table-sm table-hover data" id="data">
                                <thead class="thead-light text-center">
                                <tr>
                                    <th scope="col" >#</th>
                                    <th scope="col" >Dni</th>
                                    <th scope="col" >Participante</th>
                                    <th scope="col" >Asistencia</th>
                                    <th scope="col" >Acciones</th>
                                </tr>
                                </thead>
                                <tbody class="body-bg text-center">
                                @forelse($classroom->scheduledParticipants as $participant)
                                    <tr>
                                        <th scope="row">{{ $loop->iteration }}</th>
                                        <td>{{ $participant->dni }} /{{ $participant->pivot->id }} /{{ $participant->id }}</td>
                                        <td>
                                            <div class="widget-content p-0">
                                                <div class="widget-content-wrapper">
                                                    <div class="widget-content-left flex2">
                                                        <div class="widget-heading">{{ $participant->full_name }}</div>
                                                        @if($participant->position || $participant->area)
                                                            <div class="widget-subheading opacity-7">{{ $participant->position }} {{ $participant->area }}</div>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="form-check text-center">
                                                @if(\App\Inscription::validate($participant->pivot->id))
                                                <img src="{{asset('img/icons/check.png')}}" class="img-fluid" alt="">
                                                @else
                                                <input type="checkbox" class="form-check-input position-static" name="assistance[]" value="{{ $participant->pivot->id }}">
                                                @endif
                                            </div>
                                        </td>
                                        <td >
                                            <a href="{{ route('classrooms.assistances.index', $classroom) }}">
                                                <img src="{{asset('img/icons/lista.png')}}" alt="">
                                            </a>
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="7">
                                            <p class="text-center">No hay ninguna <f></f>hecha de asistencia disponible</p>
                                        </td>
                                    </tr>
                                @endforelse
                                </tbody>
                            </table>
                        </div>
                </div>
                    <div class="d-block text-left card-footer">
                        <button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-save"></i> Guardar</button>
                    </div>
                </form>

            </div>
        </div>
    </div>

@endsection
@section('js')
    <script type="text/javascript">
        $(document).ready(function() {
            $('#data').DataTable({

            });
        });
    </script>
@endsection


