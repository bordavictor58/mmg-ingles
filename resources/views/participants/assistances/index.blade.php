
@extends('layouts.participants.base')


@section('content')
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-actions" style="display: flex; justify-content:space-between">
                <div class="d-inline-block dropdown" >
                    <a href="{{ route('classrooms.assistances.create', [$classroom]) }}" class="btn-shadow btn btn-info">
                        <span class="btn-icon-wrapper pr-1 opacity-8">
                            <i class="fa fa-plus"></i>
                        </span>
                        CREAR ASISTENCIA
                    </a>
                </div>
                <div class="d-inline-block dropdown " >
                    <a href="{{ route('classrooms.Consolidated', [$classroom->id]) }}" class="btn btn-success"><i class="fa fa-undo-alt"></i> REGRESAR</a>
                </div>

            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12">

            <div class="main-card mb-3 card">
                <div class="card-header card-header-tab">
                    <div class="card-header-title text-capitalize">

                        {{ $classroom->name }} / {{ $classroom->start_datetime }} / {{ $classroom->end_datetime }}
                    </div>

                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-sm table-hover data" id="data">
                            <thead class="thead-light">
                            <tr>
                                <th scope="col" >#</th>
                                <th scope="col" >Lista de Asistencia</th>
                                <th scope="col" >Fecha</th>
                                <th scope="col" >Modalidad</th>
                                <th scope="col" style="width: 10%" >Acciones</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($classroom->assistances as $assistance)
                                <tr>
                                    <th scope="row">{{ $loop->iteration }}</th>
                                    <td>{{$classroom->name}}</td>

                                    <td><span class="text-nowrap">{{ $assistance->assistance_date }}</span></td>
                                    <td>
                                        @if($assistance->is_virtual == '0')
                                            <div class="badge badge-success">
                                                Presencial
                                            </div>
                                        @endif
                                        @if($assistance->is_virtual == '1')
                                            <div class="badge badge-info">
                                                Virtual
                                            </div>
                                        @endif
                                    </td>
                                    <td class="d-flex">
                                        <a class="btn btn__small__success" href="{{ route('classrooms.assistances.show',[$classroom->id, $assistance->id]) }}">Ver<i class="pe-7s-look btn-icon-wrapper"></i></a>
                                        <a class="btn btn__small__success" href="{{ route('classrooms.assistances.edit',[$classroom->id, $assistance->id]) }}">Editar<i class="pe-7s-pen btn-icon-wrapper"></i></a>

                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="7">
                                        <p class="text-center">No hay ninguna fecha de asistencia disponible</p>
                                    </td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('js')
    <script type="text/javascript">
        $(document).ready(function() {
            $('#data').DataTable({

            });
        });
    </script>
@endsection


