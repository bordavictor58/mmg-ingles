@extends('layouts.participants.base')

@section('content')

    <div class="content-title">
        <h1 class="title__primary">FINAL EXAM</h1>
    </div>

    <form action="{{ route('participant.test.register',[$classroom,$test]) }}"
          method="POST" id="formTestUser">
        <div class="col-md-12">
            <div class="body"onload="sinVueltaAtras();" onpageshow="if (event.persisted) sinVueltaAtras();" onunload="">
                @csrf
                <div class="contador-clock">
                    <div class="row inline"id="counter"></div>
                </div>
                <br>
            </div>
        </div>
        <div class="row" >

            <div class="test">

                @foreach($classroom->course->banks as $bank)
                    <div class="card" style="border: 1px solid rgb(41, 128, 185);position:relative;width:1231px; height:75px">
                        <div class="card-body">
                            <h6>{{ $loop->iteration }}.- {{ $bank->title }}</h6>
                        </div>
                    </div>

                    <div class="mb-5 mt-2">
                        @foreach($bank->childs as $child)
                            <div class="form-check my-1 mx-4">
                                <input class="form-check-input" type="radio"
                                    id="{{ $child->id }}"
                                    name="inputs[{{$child->parent_id}}]"
                                    value="{{ $child->id }}"
                                    required>
                                <label class="" for="{{ $child->id }}">
                                    {{ $child->title }}
                                    @if(Auth::user()->id==2)
                                        <span class="badge badge-primary">{{$child->is_correct}}</span>
                                    @endif
                                </label>
                            </div>
                        @endforeach
                    </div>
                @endforeach
                <input type="hidden"  name="date" value="{{$date}}">

            </div>

                <div class="col">
                        <button type="submit" id="btn-test"  class="btn btn-success">Send</button>
                </div>

        </div>

    </form>

@endsection

@section('js')
    <script>
    var numerico = '<?php echo (int)$inscription->time_course; ?>';
    var cronometro;
        $("#counter").countdown({
            digitImages: 6,
            image: "../../../../img/digits.png",
            format: 'hh:mm:ss',
            startTime: "{{ $test->time }}",
            start: true,
            timerEnd: function() { document.forms["formTestUser"].submit(); },

        });
        $('#formTestUser').submit(function(){
            $('#btn-test').prop('disabled',true);
            $('#btn-test').html('<p><i class="fa fa-spinner fa-spin fa-2x fa-fw"></i><span class="sr-only"></span> Registering...</p>');
        });
        window.history.forward();
        function sinVueltaAtras(){ window.history.forward(); }

        function __secondsToString(seconds) {
            var hour = Math.floor(seconds / 3600);
            hour = (hour < 10)? '0' + hour : hour;
            var minute = Math.floor((seconds / 60) % 60);
            minute = (minute < 10)? '0' + minute : minute;
            var second = seconds % 60;
            second = (second < 10)? '0' + second : second;
            return hour + ':' + minute + ':' + second;
        }

        function carga(){
            contador = numerico;

            cronometro = setInterval(function(){

                contador++;

                let count_convert = __secondsToString(contador);

                $.ajax({
                    url:'{{ route("courses.time",[$classroom]) }}',
                    data:{'seconds':count_convert,'integer_seconds' : contador},
                    method:'GET'
                });

                },2000);
        }

        carga();
    </script>
@endsection
