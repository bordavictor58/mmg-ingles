@extends('layouts.app')


@section('content')

        <div class="ed-item">
            <div class="container">
            <div class="content-title" style="border: 1px solid transparent">
                <h1 class="title__primary">Please take a picture of yourself</h1>
                <h3>Please select an appliance for the photo</h3>
                <div>
                    <select name="listaDeDispositivos" id="listaDeDispositivos"></select>
                    <button id="boton" class="btn btn-success btn-lg" for="labelFoto">Photo</button>
                    <p id="estado"></p>
	            </div>
            </div>

            <div class="box" style="display: flex;align-content: center;justify-content: center">
                <div class="col-md-6">
                    <div class="marcoVideo" >

                        <video muted="muted" id="video"></video>
	                    <canvas id="canvas" style="display: none;"></canvas>
                        <input id="inscription" type="hidden" value="{{$inscription->id}}">
                    </div>
                    <br>
                    <div>
                    <div class="box" style="display: flex;align-content: center;justify-content: center">
                        <a href="{{ route('participant.course.detail', $classroom)}}" id="continuar" class="btn btn-primary btn-lg" style="display: none" for="labelFoto">CONFIRM</a>
                    </div>
                    </div>

                </div>

            </div>
        </div>
        </div>
@endsection

@section('css')
<style>
    .ed-item{
        width: 100%;
        height: 100%;
    }
    #video {
        position: relative;
        top: -35px;
        width: 100%;
        height: 400px;
    }
    .marcoVideo {
        position: relative;
        width: 100%;
        height: 400px;
        border-style:solid;
        border-color:  #D01E3F;
        border-width:40px;
        background-color: burlywood;
        border-radius: 10px;
    }
</style>
@endsection
@section('js')
<script src="{{ asset('js/sayWhisky/camara.js') }}"></script>
@endsection
