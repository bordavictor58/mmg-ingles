@extends('layouts.participants.base')

@section('content')
    <div class="container">
        <div class="row" style="border: 1px solid transparent">
            <div class="d-flex">
                <img src="{{ asset('images/recuperacion.png') }}" class="content-box__img" alt="" class="">

                <img src="{{ asset('images/aprobado.png') }}" class="content-box__img d-none d-sm-block" alt="">
                <div class="content-box__button d-flex flex-column">

                    <h2 class="text-center">Congratulations, you passed the exam!</h2>

                    <img src="{{ asset('images/icon-aprobado.png') }}" class="img-fluid align-self-center my-5" style="height: 200px" alt="">
                    <div class="text-center">
                        {{-- <a href="{{ route('users.certificate.inscriptions',$inscription->id) }}" class="btn btn__info mr-1 mt-2"><img src="{{ asset('images/certificate.png') }}" width="50px" style="position: relative; right:10px;" alt="play">Certificado</a> --}}
                        <a href="{{route('home')}}" class="btn btn__info mt-2"><img src="{{ asset('images/regresar.png') }}" width="50px" style="position: relative; right:10px;" alt="play">return</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

