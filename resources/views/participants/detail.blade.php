@extends('layouts.participants.base')

@section('content')

    <div class="content-title">
        <h1 class="title__primary">{{ $classroom->course->description }}</h1>
    </div>

    <div class="content-count">
        <span class="contador_numerico">00:00:00</span>

    </div>
    <?php
    $user_agent = $_SERVER['HTTP_USER_AGENT'];

    function getBrowser($user_agent){

    if(strpos($user_agent, 'MSIE') !== FALSE)
        return 'Internet explorer';
        elseif(strpos($user_agent, 'Trident') !== FALSE) //IE 11
            return 'Internet explorer';
        else
        return null;
    }

    $navegador = getBrowser($user_agent);

    ?>
    @if($navegador)
        <div class="alert alert-danger" role="alert">
            Esta Usando El navegador Internet Explorer ! Este navegador no cuenta con soporte, Porfavor Usar Google
            Chrome Lo puedes descargar Aqui: <a href="https://n9.cl/ex7zc" target="_blank"><b>Descarga Chrome</b></a>
        </div>
    @endif
    <div class="ed-video">
        <iframe id="vimeo_video" src="https://player.vimeo.com/video/{{ $content->content_link }}?#t={{$inscription->current_time_video}}"
            style="position:absolute;top:0;left:0;width:100%;height:100%;"
            frameborder="0" allow="autoplay; fullscreen" allowfullscreen>
        </iframe>
    </div>

    <div class="content-footer" style="">
        <div class="button-group">
            <a href="{{ route('home') }}" class="btn btn__return"><img src="{{ asset('images/regresar.png') }}" width="50px" style="position: relative; right:20px;" alt="play">return</a>
            <a href="{{ route('home') }}" class="btn btn__return_true hidden"><img src="{{ asset('images/regresar.png') }}" width="50px" style="position: relative; right:20px;" alt="play">return</a>
            @isset($classroom->tests->first()->id)
                <a id="a-test"class="btn btn__info d-none"><img src="{{ asset('images/into.png') }}" width="50px" style="position: relative; right:20px;" alt="play">take an exam</a>
                <a href="{{ route('participant.course.test', [$classroom,$classroom->tests->first()]) }}" id="a-test-true"class="btn btn__info hidden"><img src="{{ asset('images/into.png') }}" width="50px" style="position: relative; right:20px;" alt="play">take an exam</a>
                <button href="{{ route('participant.course.test', [$classroom,$classroom->tests->first()]) }}" id="btn-test"class="btn btn__info disabled"><img src="{{ asset('images/into.png') }}" width="50px" style="position: relative; right:20px;" alt="play">take an exam</button>
            @endisset
        </div>
    </div>

@endsection
@section('js')
<script>
    var iframe = document.querySelector('iframe');
    var player = new Vimeo.Player(iframe);
    var numerico = '<?php echo (int)$inscription->time_course; ?>';
    var current_video = '<?php echo $inscription->current_time_video; ?>';
    var timer = '<?php echo (int)$inscription->classroom->course->timer; ?>';
    var link = '<?php echo $content->content_link ?>';
    if(typeof (localStorage['video']) === "undefined"){
        $("#vimeo_video").attr("src",`https://player.vimeo.com/video/${link}?#t=${current_video}`);
    }else{
        $("#vimeo_video").attr("src",`https://player.vimeo.com/video/${link}?#t=${localStorage.getItem('video')}`);
    }

    if(numerico >= timer){

        $('#a-test').removeClass('d-none');
        $('#btn-test').addClass('d-none');
    }else{
        $('#a-test').addClass('d-none');
        $('#btn-test').removeClass('d-none');
    }
    var cronometro;

        $(document).ready(function() {

            function currentTimeVideo() {

                player.getCurrentTime().then(function(seconds) {
                    if(Math.round(seconds)==0)
                    {
                        return false;
                    }
                    //loop
                    player.setLoop(true).then(function(loop) {

                        player.getDuration().then(function(duration) {

                            let min = secondsToString(seconds);
                            if(Math.round(seconds)== Math.round(duration))
                            {
                                console.log('finish');
                                return false;
                            }
                            localStorage.setItem('video',min);
                        });
                    });

                });
        }
        setInterval(currentTimeVideo, 5000);
        setInterval(async () => {
            const res = await guardarContadoresVideo();
            const res2 = await guardarContadoresTime();
        }, 600000);
        });
        function secondsToString(seconds) {
            var hour = Math.floor(seconds / 3600);
            hour = (hour < 10)? '0' + hour : hour;
            var minute = Math.floor((seconds / 60) % 60);
            minute = (minute < 10)? '' + minute : minute;
            var second = seconds % 60;
            second = (second < 10)? '' + second.toFixed(0) : second.toFixed(0);
            // 6m5s
            return  minute + 'm' + second + 's';
        }
        function __secondsToString(seconds) {
            var hour = Math.floor(seconds / 3600);
            hour = (hour < 10)? '0' + hour : hour;
            var minute = Math.floor((seconds / 60) % 60);
            minute = (minute < 10)? '0' + minute : minute;
            var second = seconds % 60;
            second = (second < 10)? '0' + second : second;
            return hour + ':' + minute + ':' + second;
        }
        function carga(){
            contador = numerico;

            cronometro = setInterval(function(){

                contador++;
                let count_convert = __secondsToString(contador);
                $(".contador_numerico").text(count_convert);

                if(contador >= timer){

                    $('#a-test').removeClass('d-none');
                    $('#btn-test').addClass('d-none');
                }else{
                    $('#a-test').addClass('d-none');
                    $('#btn-test').removeClass('d-none');
                }
                localStorage.setItem('time',contador);
                localStorage.setItem('time_string',count_convert);

                },1000);
        }
        function guardarContadoresVideo(){
            return $.ajax({
                url:'{{ route("video.time",[$classroom]) }}',
                data:{'seconds':localStorage.getItem('video')},
                method:'GET',
            });
        }

        function guardarContadoresTime(){
            return $.ajax({
                url:'{{ route("courses.time",[$classroom]) }}',
                data:{'seconds':localStorage.getItem('time_string'),'integer_seconds':localStorage.getItem('time')},
                method:'GET'
            });
        }
        carga();
        $(".btn__return").click(async function(){
            clearInterval(cronometro);
            const res = await guardarContadoresVideo();
            const res2 = await guardarContadoresTime();
            localStorage.clear();
            window.location.href = $(".btn__return_true").attr("href");
        });
        $("#a-test").click(async function(){
            clearInterval(cronometro);
            const res = await guardarContadoresVideo();
            const res2 = await guardarContadoresTime();
            localStorage.clear();
            window.location.href = $("#a-test-true").attr("href");
        });

</script>
@endsection

