
@extends('layouts.participants.base')


@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                    <div class="card-header ">
                        <h3 class="text-center card-title text-primary">DOCUMENT</h3>

                    </div>
                <div class="card-body text-center">
                    <div class="box">

                        @if(!$classroom)
                            @foreach ($content as $contents)
                                @if($contents->course->id==1 && $inscriptionInducc)
                                    <div class="box-child">

                                        <h5>{{$contents->name}}</h4>
                                            <a class="btn btn-sm btn-icon-only btn-outline-success"  target="_blank"
                                            href="{{asset('files/'.$contents->content_link)}}">
                                            <i class="fas fa-cloud-download-alt btn-icon-wrapper"></i> download
                                        </a>
                                    </div>
                                @endif
                                @if($contents->course->id==11 && $inscriptionTea)
                                <div class="box-child">
                                    <h5>{{$contents->name}}</h4>
                                        <a class="btn btn-sm btn-icon-only btn-outline-success"  target="_blank"
                                        href="{{asset('files/'.$contents->content_link)}}">
                                        <i class="fas fa-cloud-download-alt btn-icon-wrapper"></i> download
                                    </a>
                                </div>
                                @endif
                            @endforeach
                        @else
                            @foreach ($content as $contents)
                                <div class="box-child">
                                    <h5>{{$contents->name}}</h4>
                                    <a class="btn btn-sm btn-icon-only btn-outline-success"  target="_blank"
                                    href="{{asset('files/'.$contents->content_link)}}">
                                    <i class="fas fa-cloud-download-alt btn-icon-wrapper"></i> download
                                    </a>
                                </div>
                            @endforeach
                        @endif
                    </div>

                </div>
                <div class="card-footer text-center">
                        <a href="{{route('home')}}" class="btn btn__primary"><img src="{{ asset('images/regresar.png') }}" width="50px" style="position: relative; right:20px;" alt="play">RETURN</a>
                </div>
            </div>
        </div>
</div>

@endsection
@section('css')
<style>
.box {
        display: -ms-flex;
        display: -webkit-flex;
        display:flex;
        flex-wrap:wrap
}
.box > div {
        width: 50%;
        padding: 10px;
}

.box > div:first-child {
        margin-right: 20px;
}
</style>
@endsection



