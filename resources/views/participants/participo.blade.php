@extends('layouts.participants.base')

@section('content')
    <div class="container">
        <div class="row d-flex" style="border: 1px solid transparent;justify-content: center;">

            <div class="d-flex">
                <img src="{{ asset('images/aprobado.png') }}" class="content-box__img d-none d-sm-block" alt="">
                <div class="content-box__button d-flex flex-column">

                    <h2 class="text-center">Congratulations, you passed!</h2>
                    <img src="{{ asset('images/icon-aprobado.png') }}" class="img-fluid align-self-center my-5" style="height: 200px" alt="">
                    <div class="d-flex flex-column flex-sm-row">
                            {{-- <a href="{{ route('users.certificate.inscriptions',$inscription->id) }}" class="btn btn__info m-1"><img src="{{ asset('images/document.png') }}" width="50px" style="position: relative; right:10px;" alt="play">CHIQAQYACHIY</a> --}}
                        @if(count($classroom->course->documents) >= 1)
                            <a href="{{route('participant.course.document',[Auth::user(),$classroom])}}" class="btn btn__info m-1"><img src="{{ asset('images/documento.png') }}" width="50px" style="position: relative; right:10px;" alt="play">DOCUMENTS</a>
                        @endif
                        <a href="{{route('home')}}" class="btn btn__info m-1"><img src="{{ asset('images/regresar.png') }}" width="50px" style="position: relative; right:10px;" alt="play">RETURN</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
