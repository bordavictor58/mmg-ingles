<!doctype html>
<html lang="es">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta charset="utf-8">
    <title>Certificado</title>
    <style>
        html {
            margin: 0;
        }
        .user, .curso, .fecha, .codigo, .estado, .horas {
            position: absolute;
            margin-left: auto;
            margin-right: auto;
            left: 0;
            right: 0;
            height: auto;
            width: 70%;
            color: #373435;
        }
        .title {
            font-family: Calibri,sans-serif;
            text-transform: uppercase;
            text-align: center;
        }

        .user {
            font-family: serif;
            text-transform: uppercase;
            text-align: center;

            top: 200px;
            left: 15%;
            font-weight: bold;
            font-size: 35px;
        }

        .estado {
            top: 280px;
            left: 15%;
            font-size: 24px;
            text-transform: capitalize !important;
        }

        .curso {
            font-family: serif;
            text-transform: uppercase;
            top: 370px;
            left: 8%;
            width: 84%;
            color: #1E72B7;
            font-weight: bold;
            font-size: 35px;
        }

        .curso2 {
            top: 335px;
            left: 8%;
            width: 84%;
            color: #1E72B7;
            font-weight: bold;
            font-size: 28px;
        }

        .horas {
            font-weight: bold;
            font-family: sans-serif;
            top: 500px;
            font-size: 13px;
            left: 15%;
            text-align: center;
            text-transform: capitalize;
        }

        .fecha {
            font-family: sans-serif;
            top: 532px;
            left: 45%;
            color: #373435;
            font-size: 15px;
        }

        .codigo {
            font-family: sans-serif;
            color:white;
            top: 79px;
            left: 90%;
            font-weight: bold;
        }
    </style>

</head>
<body>
    <div style="position: fixed; left: 0; top: -265px; right: 0; bottom: 0px; text-align: center;z-index: -1000;">
            <img src="https://cumbres.ighgroup.com/img/certificates/CUMBRES.jpg" style="width: 100%; margin-top: 25%;">
    </div>
    <spam class="codigo">{{ $inscription->id }}</spam>
    <spam class="user">{{ $inscription->participant->full_name }}</spam>
    <spam class="title estado">por haber <spam style="text-transform: uppercase !important; font-weight: bold">aprobado el curso</spam></spam>
    @if($inscription->classroom->course->id == 5)

        <spam class="curso title" style="font-size: 28px;">{{ $inscription->classroom->name }}</spam>
    @else
        <spam class="curso title">{{ $inscription->classroom->name }}</spam>

    @endif
    <spam class="horas">Con una duracion de {{ $inscription->classroom->hour }} horas lectivas.</spam>
    <spam class="fecha">{{ $inscription->classroom->startDatetimeLong() }}</spam>
    <img src="data:image/svg+xml;base64,{{ base64_encode($codeQR) }}" style=" position: absolute; top: 612px; right: 180px; margin: 0; padding: 0">

</body>
</html>
