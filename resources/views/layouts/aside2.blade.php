<div class="app-sidebar sidebar-shadow">
    <div class="app-header__logo">
        <div class="logo-src"></div>
        <div class="header__pane ml-auto">
            <div>
                <button type="button" class="hamburger close-sidebar-btn hamburger--elastic" data-class="closed-sidebar">
                    <span class="hamburger-box">
                        <span class="hamburger-inner"></span>
                    </span>
                </button>
            </div>
        </div>
    </div>
    <div class="app-header__mobile-menu">
        <div>
            <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">
                <span class="hamburger-box">
                    <span class="hamburger-inner"></span>
                </span>
            </button>
        </div>
    </div>
    <div class="app-header__menu">
        <span>
            <button type="button" class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">
                <span class="btn-icon-wrapper">
                    <i class="fa fa-ellipsis-v fa-w-6"></i>
                </span>
            </button>
        </span>
    </div>
    <div class="scrollbar-sidebar">
        <div class="app-sidebar__inner">
            <ul class="vertical-nav-menu">
                <li class="app-sidebar__heading">Dashboard</li>
                <li>
                    <a href="{{route('home')}}" class="mm-active">
                        <img src="{{ asset('images/icon-seeder/inicio.png') }}" style="height: 40px; right: 15px; position:relative;" alt="">
                        Inicio
                    </a>
                </li>
                <li class="app-sidebar__heading">Gestion</li>

                @can('participant-list')
                    <li>
                        <a href="{{route('participants.index')}}">
                            <img src="{{ asset('images/icon-seeder/participantes.png') }}" style="height: 40px; right: 15px; position:relative;" alt="">
                            Participantes
                        </a>
                    </li>
                <hr>

                @endcan
                @can('course-list')
                    <li>
                        <a href="{{route('courses.index')}}">
                            <img src="{{ asset('images/icon-seeder/cursos.png') }}" style="height: 40px; right: 15px; position:relative;" alt="">
                            Cursos
                        </a>
                    </li>
                <hr>
                @endcan
                @can('course-mine')
                    <li >
                        <a href="{{ route('courses.participant.list') }}">
                            <img src="{{ asset('images/icon-seeder/cursos.png') }}" class="pull-left" style="height: 39px; right: 13px; position:relative;" alt="">
                            Mis Cursos
                        </a>
                    </li>
                <hr>

                @endcan
                @can('classroom-list')
                    <li>
                        <a href="{{route('classrooms.index')}}">
                            <img src="{{ asset('images/icon-seeder/programacion.png') }}" style="height: 40px; right: 15px; position:relative;" alt="">
                            Programacion
                        </a>
                    </li>
                <hr>

                @endcan
                @can('inscription-mine')
                    <li>
                        <a href="{{route('inscriptions.index')}}">

                            <img src="{{ asset('images/icon-seeder/inscripciones.png') }}" style="height: 40px; right: 15px; position:relative;" alt="">
                            Mis Inscripciones
                        </a>
                    </li>
                    <li>
                        <a href="{{route('report')}}">
                            <img src="{{ asset('images/icon-seeder/reportes.png') }}" style="height: 40px; right: 15px; position:relative;" alt="">
                            Reporte
                        </a>
                    </li>
                <hr>

                @endcan



            </ul>
        </div>
    </div>
</div>
