<header class="ed-container full header">
    <div class="ed-item header-container">
        <div class="nav">
            <div class="logo">
                <img src="{{ asset('images/logo-blanco.png' )}}" alt="ighgroup">
            </div>
            @auth
            <div class="user">
                <div class="user__info">
                    <button type="button" class="btn btn-success mr-2" onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">Cerrar Sesion</button>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                    </form>
                    <a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                        <img src="{{ asset('images/avatar/avatar.png') }}" alt="" class="user__img">
                    </a>

                    <div class="user__profile">
                        <span class="user__name">{{ Auth::user()->full_name }}</span>
                        <span class="user__area">{{ Auth::user()->area }}</span>
                    </div>
                </div>

            </div>
            @endauth

        </div>
    </div>
</header>
