<div class="app-sidebar sidebar-shadow">
    <div class="app-header__logo">
        <div class="logo-src"></div>
        <div class="header__pane ml-auto">
            <div>
                <button type="button" class="hamburger close-sidebar-btn hamburger--elastic" data-class="closed-sidebar">
                    <span class="hamburger-box">
                        <span class="hamburger-inner"></span>
                    </span>
                </button>
            </div>
        </div>
    </div>
    <div class="app-header__mobile-menu">
        <div>
            <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">
                <span class="hamburger-box">
                    <span class="hamburger-inner"></span>
                </span>
            </button>
        </div>
    </div>
    <div class="app-header__menu">
        <span>
            <button type="button" class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">
                <span class="btn-icon-wrapper">
                    <i class="fa fa-ellipsis-v fa-w-6"></i>
                </span>
            </button>
        </span>
    </div>
    <div class="scrollbar-sidebar">
        <div class="app-sidebar__inner">
            <ul class="vertical-nav-menu">
                <li class="app-sidebar__heading">Dashboard</li>
                <li>
                    <a href="{{route('home')}}" class="mm-active">
                        <i class="metismenu-icon pe-7s-rocket"></i>
                        Inicio
                    </a>
                </li>
                <li class="app-sidebar__heading">Gestion</li>
                @can('company-list')
                    <li>
                        <a href="{{route('companies.index')}}">
                            <i class="metismenu-icon pe-7s-diamond"></i>
                            Empresas
                        </a>
                    </li>
                @endcan
                @can('user-list')
                    <li>
                        <a href="{{route('users.index')}}">
                            <i class="metismenu-icon pe-7s-diamond"></i>
                            Usuarios
                        </a>
                    </li>
                @endcan
                @can('participant-list')
                    <li>
                        <a href="{{route('participants.index')}}">
                            <i class="metismenu-icon fa fa-users" style="color: #0155a2"></i>
                            Participantes
                        </a>
                    </li>
                @endcan
                @can('course-list')
                    <li>
                        <a href="{{route('courses.index')}}">
                            <i class="metismenu-icon pe-7s-diamond"></i>
                            Cursos
                        </a>
                    </li>
                @endcan
                @can('course-mine')
                    <li >
                        <a href="{{ route('courses.participant.list') }}">
                            <i class="metismenu-icon fa fa-book-open" style="color: #0155a2"> </i>
                            Mis Cursos
                        </a>
                    </li>
                @endcan

                @can('classroom-list')
                    <li>
                        <a href="{{route('classrooms.index')}}">
                            <i class="metismenu-icon fa fa-calendar"  style="color: #0155a2"></i>
                            Programacion
                        </a>
                    </li>
                @endcan

                @can('category-list')
                    <li>
                        <a href="{{route('categories.index')}}">
                            <i class="metismenu-icon pe-7s-diamond"  style="color: #0155a2"></i>
                            Categorias
                        </a>
                    </li>
                @endcan
                @can('certificate-list')
                    <li>
                        <a href="{{route('certificates.index')}}">
                            <i class="metismenu-icon pe-7s-diamond" style="color: #0155a2"></i>
                            Certificados
                        </a>
                    </li>
                @endcan
                @can('inscription-mine')
                    <li>
                        <a href="{{route('inscriptions.index')}}">
                            <i class="metismenu-icon fa fa-calendar-check" style="color: #0155a2"></i>
                            Mis Inscripciones
                        </a>
                    </li>
                    <li>
                        <a href="{{route('report')}}">
                            <img src="{{ asset('images/icon-seeder/BOTONES-03.jpg') }}" style="height: 27px" alt="">
                            <i class="metismenu-icon fa fa-file-archive" style="color: #0155a2"></i>
                            Reporte
                        </a>
                    </li>
                @endcan
                @can('certificate-mine')
                    <li>
                        <a href="{{route('users.certificate')}}">
                            <i class="metismenu-icon fa fa-id-card" style="color: #0155a2"></i>
                            Mis Certificados
                        </a>
                    </li>
                @endcan
                @can('role-list')
                    <li>
                        <a href="{{route('roles.index')}}">
                            <i class="metismenu-icon pe-7s-diamond"></i>
                            Roles
                        </a>
                    </li>
                @endcan
                @can('bank-list')
                    <li>
                        <a href="{{route('banks.index')}}">
                            <i class="metismenu-icon pe-7s-diamond"></i>
                            Banco de preguntas
                        </a>
                    </li>
                @endcan
                @can('type-list')
                    <li>
                        <a href="{{route('types.index')}}">
                            <i class="metismenu-icon pe-7s-diamond"></i>
                            Tipo de Contenido
                        </a>
                    </li>
                @endcan
                @can('forum-list')
                <li>
                    <a href="{{route('forums.index')}}">
                        <i class="metismenu-icon fa fa-question" style="color: #0155a2"></i>
                       Preguntas
                    </a>
                </li>
                @endcan

            </ul>
        </div>
    </div>
</div>
