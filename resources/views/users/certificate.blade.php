
@extends('layouts.participants.base')

@section('content')

    <div class="row d-flex justify-content-center align-content-center flex-column" style="border: 1px solid transparent">
        <h1 class="title__primary">Mis Certificados</h1>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-sm table-hover">
                            <thead class="thead-light">
                            <tr  class="text-center">

                                <th>NRO</th>
                                <th>CURSO</th>
                                <th>PERSONA</th>
                                <th>NOTA FINAL</th>
                                <th>CLASSROOM</th>
                                <th>FECHA</th>
                                <th>OPCIONES</th>
                            </tr>
                            </thead>
                            @foreach ($user->inscriptions as $inscription)
                            <tr>

                                <td>NRo</td>
                                <td>{{$inscription->classroom->course->name}}</td>
                                <td>{{$inscription->participant->full_name}}</td>
                                <td>{{$inscription->grade}}</td>
                                <td>{{$inscription->classroom->id}}</td>
                                <td>{{$inscription->classroom->end_datetime}}</td>
                                <td style="white-space:nowrap;">
                                    @if($inscription->grade_tried === null)
                                        <span class="text-black">Sin intentos</span>
                                    @else
                                        @if($inscription->grade >= $inscription->grade_min )
                                            <a class="btn btn-sm btn-success"
                                               href="{{ route('users.certificate.inscriptions', $inscription->id) }}">
                                                Certificado
                                            </a>
                                        @else
                                            <span class="text-danger">Desaprobado</span>
                                        @endif

                                    @endif

                                </td>
                            </tr>
                        @endforeach
                        </table>
                    </div>
                </div>
            </div>
            <br>
            <div class="col-md-4">
                <a href="{{ route('home') }}" class="btn btn__info"><img src="{{ asset('images/regresar.png') }}" width="50px" style="position: relative; right:10px;" alt="play"> Regresar</a>

            </div>
        </div>
    </div>

@endsection



