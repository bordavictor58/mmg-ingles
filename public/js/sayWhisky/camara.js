/*
    Tomar una fotografía y guardarla en un archivo v3
    @date 2018-10-22
    @author parzibyte
    @web parzibyte.me/blog
*/
function tieneSoporteUserMedia() {
	return !!(navigator.getUserMedia || ( navigator.getUserMedia ||
                       navigator.webkitGetUserMedia ||
                       navigator.mozGetUserMedia) || navigator.webkitGetUserMedia || navigator.msGetUserMedia)
}
function _getUserMedia() {
	return (navigator.getUserMedia ||
        navigator.webkitGetUserMedia ||
        navigator.mozGetUserMedia ||
        navigator.msGetUserMedia).apply(navigator, arguments);
}

// Declaramos elementos del DOM
const $video = document.querySelector("#video"),
	$canvas = document.querySelector("#canvas"),
	$boton = document.querySelector("#boton"),
	$estado = document.querySelector("#estado"),
	$listaDeDispositivos = document.querySelector("#listaDeDispositivos");

// La función que es llamada después de que ya se dieron los permisos
// Lo que hace es llenar el select con los dispositivos obtenidos
const llenarSelectConDispositivosDisponibles = () => {

	navigator
		.mediaDevices
		.enumerateDevices()
		.then(function (dispositivos) {
            console.log(dispositivos);
			const dispositivosDeVideo = [];
			dispositivos.forEach(function (dispositivo) {
                console.log(dispositivo);
				const tipo = dispositivo.kind;
				if (tipo === "videoinput") {
					dispositivosDeVideo.push(dispositivo);
				}
			});

			// Vemos si encontramos algún dispositivo, y en caso de que si, entonces llamamos a la función
			if (dispositivosDeVideo.length > 0) {
				// Llenar el select
				dispositivosDeVideo.forEach(dispositivo => {
					const option = document.createElement('option');
					option.value = dispositivo.deviceId;
					option.text = dispositivo.label;
					$listaDeDispositivos.appendChild(option);
					console.log("$listaDeDispositivos => ", $listaDeDispositivos)
				});
			}
		});
}

(function () {
	// Comenzamos viendo si tiene soporte, si no, nos detenemos
    console.log(tieneSoporteUserMedia());
	if (!tieneSoporteUserMedia()) {
		alert("Lo siento. Tu navegador no soporta esta característica");
		$estado.innerHTML = "Parece que tu navegador no soporta esta característica. Intenta actualizarlo.";
		return;
	}
	//Aquí guardaremos el stream globalmente
	let stream;


	// Comenzamos pidiendo los dispositivos
	navigator
		.mediaDevices
		.enumerateDevices()
		.then(function (dispositivos) {
			// Vamos a filtrarlos y guardar aquí los de vídeo
			const dispositivosDeVideo = [];

			// Recorrer y filtrar
			dispositivos.forEach(function (dispositivo) {
				const tipo = dispositivo.kind;
				if (tipo === "videoinput") {
					dispositivosDeVideo.push(dispositivo);
				}
			});

			// Vemos si encontramos algún dispositivo, y en caso de que si, entonces llamamos a la función
			// y le pasamos el id de dispositivo
			if (dispositivosDeVideo.length > 0) {
				// Mostrar stream con el ID del primer dispositivo, luego el usuario puede cambiar
				mostrarStream(dispositivosDeVideo[0].deviceId);
			}
		});



	const mostrarStream = idDeDispositivo => {
		_getUserMedia(
			{
				video: true,
                audio:false
			},
			function (streamObtenido) {
				// Aquí ya tenemos permisos, ahora sí llenamos el select,
				// pues si no, no nos daría el nombre de los dispositivos
				llenarSelectConDispositivosDisponibles();

				// Escuchar cuando seleccionen otra opción y entonces llamar a esta función
				$listaDeDispositivos.onchange = () => {
					// Detener el stream
					if (stream) {
						stream.getTracks().forEach(function (track) {
							track.stop();
						});
					}
					// Mostrar el nuevo stream con el dispositivo seleccionado
					mostrarStream($listaDeDispositivos.value);
				}

				console.log(streamObtenido);
				// Simple asignación
				stream = streamObtenido;

				// Mandamos el stream de la cámara al elemento de vídeo
				$video.srcObject = stream;
				$video.play();

				//Escuchar el click del botón para tomar la foto
				$boton.addEventListener("click", function () {

					//Pausar reproducción
					$video.pause();

					//Obtener contexto del canvas y dibujar sobre él
					let contexto = $canvas.getContext("2d");
					$canvas.width = $video.videoWidth;
					$canvas.height = $video.videoHeight;
					contexto.drawImage($video, 0, 0, $canvas.width, $canvas.height);

					let foto = $canvas.toDataURL(); //Esta es la foto, en base 64
					$estado.innerHTML = "Please wait, loading";
                    const token = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
                    const valor = document.querySelector('#inscription');

                    function dataURLtoFile(dataurl, filename) {

                        var arr = dataurl.split(','),
                            mime = arr[0].match(/:(.*?);/)[1],
                            bstr = atob(arr[1]),
                            n = bstr.length,
                            u8arr = new Uint8Array(n);

                        while(n--){
                            u8arr[n] = bstr.charCodeAt(n);
                        }

                        return new File([u8arr], filename, {type:mime});
                    }
                    var file = dataURLtoFile(foto,'hello.png');
                    console.log('file');
                    const formData = new FormData()
                    formData.append('myFile', file)

					fetch("/u/courses/video/save/"+valor.value, {
						method: "POST",
						body: formData,
						headers: {
                            "X-CSRF-TOKEN": token,
						}
					})
						.then(resultado => {
							// A los datos los decodificamos como texto plano
							return resultado.text()
						})
						.then(nombreDeLaFoto => {
							// nombreDeLaFoto trae el nombre de la imagen que le dio PHP
							console.log("La foto fue enviada correctamente");
							$estado.innerHTML = `Success`;

                            document.getElementById("continuar").style.display="block";

							//$estado.innerHTML = `Foto guardada con éxito. Puedes verla <a target='_blank' href=''> aquí</a>`;
						}).catch(function(response) {
                            //se ejecuta cuando hay un error
                            //response: mensaje de error del api
                            alert('error al capturar la foto !')
                            console.log(response)
                        })

					//Reanudar reproducción
					$video.play();
				});
			}, function (error) {
				console.log("Permiso denegado o error: ", error);
				$estado.innerHTML = "No se puede acceder a la cámara, o no diste permiso.";
			});
	}
})();
