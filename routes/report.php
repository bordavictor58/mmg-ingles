<?php

use Illuminate\Support\Facades\Route;

// REPORTS
route::get('report','ReportController@index')->name('report');
route::post('exportExcel','ReportController@exportExcel')->name('exportExcel');
route::get('exportExcel/Grade','ReportController@exportDetailGrade')->name('exportDetailGrade');
route::get('exportExcel/GradeBegin','ReportController@exportDetailGradeBegin')->name('exportDetailGradeBegin');
//reportes de usuario
route::get('u/courses/report/general','ReportController@reportGeneral')->name('participant.report');
route::post('u/courses/report/assistance','ReportController@reportAssistance')->name('report.assistance');
route::post('u/courses/report/general','ReportController@ReportAcademygeneral')->name('report.general');
route::post('u/courses/report/history/Learning','ReportController@reportHistoryGeneral')->name('report.historyLearning');
