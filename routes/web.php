<?php

use Illuminate\Support\Facades\Route;


Route::get('/', function () {
    return view('auth.login');
});


Route::get('/home', 'HomeController@index')->name('home');

Route::get('/u/courses/list/category', 'Participant\ParticipantController@listCategory')->name('participant.course.listCategory');
Route::get('/u/courses/{category}/list', 'Participant\ParticipantController@list')->name('participant.course.list');
Route::get('/u/courses/{classroom}', 'Participant\ParticipantController@detail')->name('participant.course.detail');
Route::get('/u/courses/sayCheese/{classroom}/cheers', 'Participant\ParticipantController@saycheese')->name('participant.course.saycheese');
Route::get('/u/{user}/courses/{classroom?}', 'Participant\ParticipantController@document')->name('participant.course.document');
Route::post('/u/courses/{classroom}/test/{test}/register', 'Participant\ParticipantController@register')->name('participant.test.register');
Route::get('/u/certificates', 'Participant\ParticipantController@certificates')->name('participant.course.certificates');
Route::get('/user/courses/{classroom}/test_user/{test_user?}/note/participant', 'Participant\ParticipantController@note')->name('participant.course.note');
Route::get('/user/courses/{classroom}/note/nofound', 'Participant\ParticipantController@noteNofound')->name('participant.course.note.nofound');
Route::get('/u/courses/{classroom}/test/{test_user}/result', 'Participant\ParticipantController@result')->name('participant.course.results');
Route::get('u/courses/{classroom}/encuesta/{test_user}','Participant\ParticipantController@encuesta')->name('users.encuesta');
Route::get('/u/courses', 'Participant\CourseController@index')->name('participant.course.index');
Route::get('/courses/detail/{classroom}', 'Participant\CourseController@getDetail')->name('course.detail');





Route::get('/u/courses/{classroom}/test/{test}', 'Participant\ParticipantController@test')->name('participant.course.test');

//
Route::get('/certificate', 'User\CertificateController@index')->name('users.certificate');
Route::get('/certificate/{inscription}', 'User\CertificateController@create')->name('users.certificate.inscriptions');

Auth::routes();
Route::post('/register/company', 'Contractor\RegisterController@store')->name('register.company');

Route::resource('participants','ParticipantController');
Route::get('participants/{user}/modify', 'ParticipantController@modify')->name('participants.modify');
Route::get('participants/{user}/profile', 'ParticipantController@profile')->name('participants.profile');
Route::put('participant/{user}', 'ParticipantController@actualize')->name('participants.actualize');
Route::post('participants/import-excel','ParticipantController@import')->name('participants.import');


Route::resource('facilitators','FacilitadorController');
Route::get('facilitators/{user}/modify', 'FacilitadorController@modify')->name('facilitators.modify');
Route::put('facilitator/{user}', 'FacilitadorController@actualize')->name('facilitators.actualize');
Route::resource('companies','CompanyController');


Route::resource('categories','CategoryController');

Route::get('courses/u', 'Courses\ParticipantsController@list')->name('courses.participant.list');
Route::get('courses/{classroom}/u/{user}', 'Courses\ParticipantsController@detail')->name('courses.participant.detail');
Route::get('courses/{classroom}/types/{type}/u', 'Courses\ParticipantsController@list_type')->name('courses.participant.type');
Route::get('courses/{classroom}/types/{type}/contents/{content}/u/play', 'Courses\ParticipantsController@play')->name('courses.participant.play');
Route::resource('courses.contents','Courses\CourseContentController');
Route::get('courses/{course}/types/{type}/contents/{content}/u/play', 'Courses\ParticipantsController@play')->name('courses.participant.play');

Route::resource('courses.types','Courses\CourseTypeController');
Route::resource('courses.types.contents','Courses\CourseTypeContentController');
Route::get('courses/contents/{media}/download','Courses\CourseTypeContentController@download_content')->name('contents.download');

//Route::get('courses/contents/{course}', 'Courses\ContentController@index')->name('courses.contents.index');
Route::resource('courses','CourseController');
Route::resource('courses.certificates','Courses\CourseCertificateController');


Route::get('classrooms/{classroom}/test/{key?}','Classroom\TestController@list')->name('classrooms.list');
Route::resource('classrooms.tests','Classroom\TestController');
Route::resource('classrooms','ClassroomController');
Route::get('classrooms/{classroom}/consolidated', 'Classroom\ClassroomConsolidatedController@consolidated')
    ->name('classrooms.Consolidated');
Route::resource('classrooms.meetings','Classroom\MeetingController');
Route::resource('classrooms.assistances','Classroom\ClassroomAssistanceController');
Route::post('classrooms/{classroom}/assistances/register',
    'Classroom\ClassroomAssistanceController@register')
    ->name('classrooms.assistances.register');

//asistances en facilitator

Route::get('bank/{course}','BankController@save')->name('banks.save');
Route::get('banks/{course?}/module{module?}','BankController@list')->name('banks.list');
Route::resource('banks','BankController');
Route::post('banks/import-excel/{course}','BankController@import')->name('banks.import');;



Route::resource('roles','RoleController');
Route::resource('types','TypeController');
Route::resource('certificates','CertificateController');
Route::resource('users','UserController');
Route::get('courses/{classroom}/test/{test}','Course\CourseTestController@test')->name('courses.test');
Route::get('courses/{classroom}/encuesta/{test_user}','Course\CourseTestController@encuesta')->name('tests.encuesta');
Route::post('courses/{classroom}/test/{test}/register','Course\CourseTestController@register')->name('courses.test.register');
Route::get('courses/{classroom}/test/{test_user}/result','Course\CourseTestController@result')->name('courses.test.result');
Route::get('courses/{classroom}/test/{test_user}/detail','Course\CourseTestController@detail')->name('courses.test.detail');
Route::resource('users.testusers','User\TestUserController');


Route::resource('inscriptions','InscriptionController');
Route::post('inscriptions/import-excel','InscriptionController@import')->name('inscriptions.import');
Route::get('inscriptions/{classroom}/register','InscriptionController@register')->name('inscriptions.register');
Route::post('inscriptions/{classroom}','InscriptionController@save')->name('inscriptions.save');
Route::get('inscriptions/{classroom}/detail','InscriptionController@detail')->name('inscriptions.detail');

Route::resource('classrooms.tests','Classroom\TestController');
Route::get('classrooms.tests/{classroom}/list','Classroom\TestController@list')->name('tests.list');
Route::resource('courses.certificates','Courses\CourseCertificateController');

Route::get('modules/{course}','ModuleController@index')->name('modules.index');
route::get('modules/{course}/create','ModuleController@create')->name('modules.create');
Route::get('module/{module?}/courses/{course?}','ModuleController@show')->name('modules.show');
route::get('modules/{module}/edit','ModuleController@edit')->name('modules.edit');
route::post('modules/{course}','ModuleController@store')->name('modules.store');
route::put('modules/{module}','ModuleController@update')->name('modules.update');
Route::delete('modules/{module}','ModuleController@destroy')->name('modules.destroy');

//videos


//Question
Route::resource('forums','ForumController');
Route::get('courses/{classroom}/videos','ClassroomController@updateTimeVideo')->name('video.time');
Route::get('courses/{classroom}/video','ClassroomController@updateTimeCourse')->name('courses.time');
Route::post('u/courses/video/save/{inscription}','ClassroomController@videoSave')->name('video.save');
//has video guia

Route::get('participants/has/videoGuia/{classroom}','ClassroomController@hasVideoGuia')->name('participants.video.guia');
